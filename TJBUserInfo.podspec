
Pod::Spec.new do |s|
  s.name             = 'TJBUserInfo'
  s.version          = '0.1'
  s.summary          = 'A short description of TJBUserInfo.'
 
  s.description      = <<-DESC
							需要在AppDelegate里边添加微信注册微信代理，并且在info里边添加微信调起白名单以及URLScheme.
                       DESC

  s.homepage         = 'http://gitlab.it.ikang.com/jinhu.zhang' 
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'HU' => 'jinhu.zhang@ikang.com' }
  s.source           = { :git => 'http://gitlab.it.ikang.com/ios/TJBUserInfo.git', :tag => s.version.to_s }
 
  s.platform         = :ios, "9.0"
  s.ios.deployment_target = "9.0"

  s.requires_arc        = true
 
   s.dependency 'IKCoreModule'    
   s.dependency 'IKToolsModule'
   s.dependency 'IKResourcesModule'
   s.dependency 'IQKeyboardManager'
   s.dependency 'RKNotificationHub'
   s.dependency 'CYLTabBarController'


   s.public_header_files = '/TJBUserInfo/TJBUserInfo/TJBUserInfo/Class/TJBUserInfoViewController.h'
   s.source_files =        '/TJBUserInfo/TJBUserInfo/TJBUserInfo/Class/TJBUserInfoViewController.{h,m}'
   
    s.subspec 'Model' do |ss| 
          ss.source_files          = 'TJBUserInfo/TJBUserInfo/TJBUserInfo/Class/Model/**/*' 
          ss.public_header_files   = 'TJBUserInfo/TJBUserInfo/TJBUserInfo/Class/Model/*.{h}'
    end
    s.subspec 'Presenter' do |ss| 
    	  ss.dependency 'TJBUserInfo/Model'
          ss.source_files          = 'TJBUserInfo/TJBUserInfo/TJBUserInfo/Class/Presenter/**/*' 
          ss.public_header_files   = 'TJBUserInfo/TJBUserInfo/TJBUserInfo/Class/Presenter/*.{h}'
    end
    s.subspec 'View' do |ss|
    	  ss.dependency 'TJBUserInfo/Model'
          ss.source_files          = 'TJBUserInfo/TJBUserInfo/TJBUserInfo/Class/View/**/*' 
          ss.public_header_files   = 'TJBUserInfo/TJBUserInfo/TJBUserInfo/Class/View/*.{h}'
    end
    s.subspec 'Category' do |ss|
    	  ss.dependency 'TJBUserInfo/Model'
          ss.source_files          = 'TJBUserInfo/TJBUserInfo/TJBUserInfo/Class/Category/**/*' 
          ss.public_header_files   = 'TJBUserInfo/TJBUserInfo/TJBUserInfo/Class/Category/*.{h}'
    end

end
