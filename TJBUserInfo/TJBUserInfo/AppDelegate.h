//
//  AppDelegate.h
//  TJBUserInfo
//
//  Created by 薛健 on 2019/7/1.
//  Copyright © 2019 iKang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

