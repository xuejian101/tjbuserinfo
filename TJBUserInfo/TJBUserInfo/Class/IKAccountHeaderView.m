//
//  IKAccountHeaderView.m
//  Leaf
//
//  Created by zramals on 16/5/4.
//  Copyright © 2016年 ikang. All rights reserved.
//

#import "IKAccountHeaderView.h"
#import <Masonry.h>
#import <IKToolsModule.h>
@interface IKAccountHeaderView ()
@end

@implementation IKAccountHeaderView

-(instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor =[UIColor whiteColor];
        _headButton  = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_headButton];
        [_headButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left);
            make.top.mas_equalTo(0);
            make.right.equalTo(self.mas_right);
            make.height.mas_equalTo(120);
        }];
        _headButtonImageView = [[UIImageView alloc] init];
        _headButtonImageView.image =[UIImage imageNamed:@"image_touxiang"];
        [_headButton addSubview:_headButtonImageView];
        [_headButtonImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headButton).offset(15);
            make.centerY.equalTo(_headButton.mas_centerY);
        }];
        [_headButtonImageView setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
        _userName = [[UILabel alloc] init];
        _userName.lineBreakMode =NSLineBreakByTruncatingTail;
        _userName.numberOfLines =1;
        _userName.font =[UIFont boldSystemFontOfSize:20];
        _userName.textColor =[UIColor baseTextColor];
        [_headButton addSubview:_userName];

        UILabel *userDes = [[UILabel alloc] init];
        userDes.text =@"编辑个人信息";
        userDes.font =[UIFont systemFontOfSize:14];
        userDes.textColor =[UIColor baseTextGrayColor];
        [_headButton addSubview:userDes];

        UIImageView *arrow = [[UIImageView alloc] init];
        arrow.image =[UIImage imageNamed:@"ic_arrow_gray_right"];
        [_headButton addSubview:arrow];
        [arrow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_headButton.mas_right).offset(-15);
            make.centerY.equalTo(_headButton.mas_centerY);
        }];
        [_userName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_headButton.mas_centerY);
            make.left.equalTo(_headButtonImageView.mas_right).offset(20);
            make.right.equalTo(arrow.mas_right).offset(-20);
            make.width.priorityLow();
        }];
        [userDes mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_userName.mas_bottom).offset(10);
            make.left.equalTo(_userName.mas_left);
        }];
    }
    return self;
}
 
@end
