//
//  IKAccountHeaderView.h
//  Leaf
//
//  Created by zramals on 16/5/4.
//  Copyright © 2016年 ikang. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface IKAccountHeaderView : UIImageView

@property (nonatomic ,strong)UIButton *headButton;
@property (nonatomic ,strong)UIImageView *headButtonImageView;
@property (nonatomic ,strong)UILabel *userName;

@property (nonatomic ,strong)UIButton *cardButton;
@property (nonatomic ,strong)UIButton *couponButton;

@property (nonatomic ,strong)UILabel *cardMoneyLabel;
@property (nonatomic ,strong)UILabel *couponCountLabel;


@end
