//
//  TJBUserInfoViewController.m
//  TJBUserInfo
//
//  Created by 薛健 on 2019/7/1.
//  Copyright © 2019 iKang. All rights reserved.
//

#import "TJBUserInfoViewController.h"
#import "UIImage+Loader.h"
#import "IKNewNormalStyleCell.h"
#import "IKAccountControllerCell.h"
#import "IKAccountHeaderView.h"
#import "NSString+Keychain.h"
#import <IKToolsModule.h>
#import <IKMBaseModel.h>
#import <Category.h>
#import <IKCoreModule.h>
#import "IKMUserMePresenter.h"
#import <RKNotificationHub.h>
#import <UIViewController+CYLTabBarControllerExtention.h>
@interface TJBUserInfoViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) IKAccountHeaderView * headerView;
@property (nonatomic,strong) IKMUserMePresenter *presenter;
@property (nonatomic,strong) IKCardNumModel *allCardModel;
@property (nonatomic,strong) IKHomeNewMessageModel *messageModel;
@property (nonatomic,strong) RKNotificationHub *hub;

@property (nonatomic,strong) UITableView * tableView;
@property (nonatomic,strong) NSArray * dataArray;
@property (nonatomic,assign) BOOL isScrollEnd;
@property (nonatomic,strong) UIButton * rightTitleButton;
@property (nonatomic,strong) UIButton * settingButton;
@property (nonatomic,strong) UIButton * idfaButton;

@end

@implementation TJBUserInfoViewController
#pragma mark - 网络请求
- (void)startRequest{
    if([UserDefaults boolForKey:@"isLogin"]) {
        [self setGetTotalMoneyAndNums];
        [self setGetNewHomeMessage];
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bg"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar resetLine];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = YES;
    self.edgesForExtendedLayout = UIRectEdgeLeft | UIRectEdgeBottom | UIRectEdgeRight;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    [self startRequest];
    
    if ([UserDefaults boolForKey:@"isLogin"]){
        if ([UserDefaults valueForKey:@"userInfo"] &&[UserDefaults valueForKey:@"userInfo"][@"name"] ){
            NSMutableString * nameStr = [NSMutableString stringWithString:[UserDefaults valueForKey:@"userInfo"][@"name"]];
            for(int i = 0; i < nameStr.length; i++) {
                if(i == 0) {
                    continue;
                }
                [nameStr replaceCharactersInRange:NSMakeRange(i, 1) withString:@"*"];
            }
            self.headerView.userName.text = [NSString stringWithString:nameStr];
        }else{
            _headerView.userName.text = @"体检宝用户";
        }
        
        if([UserDefaults valueForKey:@"userInfo"][@"sex"]){
            NSString *sexString = [NSString stringWithFormat:@"%@",[UserDefaults valueForKey:@"userInfo"][@"sex"]];
            switch ([sexString intValue]){
                case 1:
                    _headerView.headButtonImageView.image = [UIImage imageNamed:@"headImage_man"];
                    break;
                case 0:
                    _headerView.headButtonImageView.image = [UIImage imageNamed:@"headImage_woman"];
                    break;
                default:
                    _headerView.headButtonImageView.image = [UIImage imageNamed:@"image_touxiang"];
                    break;
            }
        }else{
            _headerView.headButtonImageView.image = [UIImage imageNamed:@"image_touxiang"];
        }
    }else{
        _headerView.userName.text = @"点击登录";
        _headerView.headButtonImageView.image = [UIImage imageNamed:@"image_touxiang"];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [BaseMediator actionMethodFromClass:@"MobClick" Selector:@"event:" Prarms:@[@"wo"]];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightTitleButton];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.settingButton];
    [self setUpTableView];
}

-(void)dealloc{

}
- (void)addNotification{
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotification:) name:@"notification_login_name_refresh" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataAccount) name:@"NotificationLogoutAction" object:nil];
}

-(void)setGetTotalMoneyAndNums
{
    WS(ws)
    [self.presenter getTotalMoneyAndNumsBlock:^(IKCardNumModel * _Nullable dataModel) {
        NSLog(@"%@",dataModel);
        ws.allCardModel =dataModel;
        [ws.tableView reloadData];
    }];
}

-(void)setGetNewHomeMessage
{
    WS(ws)
    [self.presenter getNewHomeMessageBlock:^(IKHomeNewMessageModel * _Nullable dataModel) {
        NSLog(@"%@",dataModel);
        ws.messageModel = dataModel;
        if (ws.messageModel.messageCount.integerValue>0) {
            [ws.hub increment];
            [ws.hub pop];
        }else{
            [ws.hub decrement];
        }
    }];
}

-(void)setUpTableView
{
    _tableView = [[UITableView alloc] init];
    _tableView.separatorStyle = UITableViewStyleGrouped;
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, CGFLOAT_MIN)];
    _tableView.backgroundColor =[UIColor whiteColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor =[UIColor baseBackgroundColor];
    [_tableView registerClass:[IKAccountControllerCell class] forCellReuseIdentifier:NSStringFromClass([IKAccountControllerCell class])];
    [_tableView registerClass:[IKNewNormalStyleCell class] forCellReuseIdentifier:NSStringFromClass([IKNewNormalStyleCell class])];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom).offset(-49);
    }];
    
    _headerView = [[IKAccountHeaderView alloc] init];
    [_headerView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 120)];
    [_headerView setUserInteractionEnabled:YES];
    [_headerView setContentMode:UIViewContentModeScaleAspectFill];
    [_headerView setClipsToBounds:YES];
    WS(ws)
    [_headerView.headButton addActionforControlEvents:UIControlEventTouchUpInside Completion:^{
        [BaseMediator actionMethodFromClass:@"MobClick" Selector:@"event:" Prarms:@[@"mine_my_message"]];
        if (![UserDefaults boolForKey:@"isLogin"]){
            [BaseMediator basePresent:@"IKLoginMainViewController" dic:nil];
        }else{
            [ws jumpToCommonMessage];
        }
    }];
    
    _tableView.tableHeaderView = self.headerView;
    
//    _headerView.userName.text = @"点击登录";
//    _headerView.headButtonImageView.image = [UIImage imageNamed:@"image_touxiang"];

#if DEBUG
    _tableView.tableFooterView = self.idfaButton;//查看idfa
#else
    
#endif
    
}
- (UIButton *)rightTitleButton{
    if (!_rightTitleButton) {
        _rightTitleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightTitleButton.frame = CGRectMake(0, 0, 24, 30);
        [_rightTitleButton setImage:[UIImage imageNamed:@"nav_ic_message"] forState:UIControlStateNormal];
        _rightTitleButton.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 0);
        WS(ws)
        [_rightTitleButton addActionforControlEvents:UIControlEventTouchUpInside Completion:^{
            [ws.hub decrement];
            [BaseMediator actionMethodFromClass:@"MobClick" Selector:@"event:" Prarms:@[@"IKang_MyAccount_Message"]];
            if ([UserDefaults boolForKey:@"isLogin"]){
                 [BaseMediator basePush:@"IKMessageViewController" dic:@{@"hidesBottomBarWhenPushed":@1}];
            }else{
                void (^success)(void) = ^{
                    if ([UserDefaults boolForKey:@"isLogin"]) {
                        [BaseMediator basePush:@"IKMessageViewController" dic:@{@"hidesBottomBarWhenPushed":@1}];
                    }
                };
                [BaseMediator basePresent:@"IKLoginMainViewController" dic:@{@"loginSuccessBlock" : success}];
            }
        }];
        
        _hub = [[RKNotificationHub alloc]initWithView:_rightTitleButton];
        [_hub moveCircleByX:1 Y:3];
        [_hub scaleCircleSizeBy:0.33];
        [_hub hideCount];
        
    }
    return _rightTitleButton;
}

- (UIButton *)settingButton{
    if (!_settingButton) {
        _settingButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _settingButton.frame = CGRectMake(0, 5, 24, 24);
        [_settingButton setImage:[UIImage imageNamed:@"nav_ic_setting"] forState:UIControlStateNormal];
        _settingButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -8);
        
        [_settingButton addActionforControlEvents:UIControlEventTouchUpInside Completion:^{
            [BaseMediator basePush:@"IKAccountSettingViewController" dic:@{@"hidesBottomBarWhenPushed":@1}];
        }];
        
    }
    return _settingButton;
}

//跳转头像
-(void)jumpToCommonMessage{
    [BaseMediator actionMethodFromClass:@"MobClick" Selector:@"event:" Prarms:@[@"IKang_MyAccount_MyInfo"]];
    [BaseMediator basePush:@"IKPersonalInfoListController" dic:@{@"hidesBottomBarWhenPushed":@1}];
}

- (void)handleNotification:(NSNotification *) notification{
    if (notification.object && [notification.object isKindOfClass:[NSString class]]){
        _headerView.userName.text = notification.object;
    }
}

- (void)reloadDataAccount{
    self.allCardModel = nil;
    [self.tableView reloadData];
}

#pragma mark - DELEGATE
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.dataArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *array =self.dataArray[section];
    return [array count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55.0f;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 2) {
        IKNewNormalStyleCell *cell111 = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IKNewNormalStyleCell class])];
        [cell111.titleLabel setText:self.dataArray[indexPath.section][indexPath.row][@"title"]];
        cell111.detailLabel.attributedText = _headerView.couponCountLabel.attributedText;
        return cell111;
    }else {
        IKAccountControllerCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([IKAccountControllerCell class])];
        [cell setMarginMidLineWith:tableView IndexPath:indexPath CellHeight:0 Margin:15 isBothSideMargin:YES];
        [cell.titleLabel setText:self.dataArray[indexPath.section][indexPath.row][@"title"]];
        [cell.iconView setImage:[UIImage imageNamed:self.dataArray[indexPath.section][indexPath.row][@"icon"]]];
        cell.detailLabel.attributedText = _headerView.couponCountLabel.attributedText;
//        if (self.allCardModel.results.count > 0){
//            IKCardNumModel *mode = self.allCardModel.results[0];
            if (indexPath.section == 0 && indexPath.row == 0){
                if (self.allCardModel.totalMoney) {
                    cell.detailLabel.alpha = 1;
                    cell.detailLabel.attributedText = [self getAttributedString:@"张" count:self.allCardModel.couponsNum];//爱康优惠券
                }else{
                    cell.detailLabel.alpha = 0;
                }
            }
//        }else{
//            cell.detailLabel.alpha = 0;
//        }
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (![UserDefaults boolForKey:@"isLogin"]){
            void (^success)(void) = ^{
                if ([UserDefaults boolForKey:@"isLogin"]) {
                    [self jumpToCoupon];
                }
            };
            [BaseMediator actionMethodFromClass:@"MobClick" Selector:@"event:" Prarms:@[@"my_coupon"]];
            [BaseMediator basePresent:@"IKLoginMainViewController" dic:@{@"loginSuccessBlock" : success}];
            
        }else{
            [self jumpToCoupon];
        }
    }else if (indexPath.section == 1){
        if (indexPath.row == 0){
//            //我的报告
            [BaseMediator actionMethodFromClass:@"MobClick" Selector:@"event:" Prarms:@[@"mine_my_report"]];
            if (![UserDefaults boolForKey:@"isLogin"]){
                void (^success)(void) = ^{
                    if ([UserDefaults boolForKey:@"isLogin"]) {
                        [[UIViewController new] cyl_popSelectTabBarChildViewControllerAtIndex:2];
                    }
                };
                [BaseMediator basePresent:@"IKLoginMainViewController" dic:@{@"loginSuccessBlock" : success}];
                
            }else{
                [[UIViewController new] cyl_popSelectTabBarChildViewControllerAtIndex:2];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"Report_GoBackRoot" object:nil];
            }
        }
        if (indexPath.row == 1){
//            // 我的订单
//            [MobClick event:@"mine_my_order"];
            [BaseMediator actionMethodFromClass:@"MobClick" Selector:@"event:" Prarms:@[@"mine_my_order"]];

            if (![UserDefaults boolForKey:@"isLogin"]){
//                IKLoginMainViewController *loginVC = [[IKLoginMainViewController alloc]initWithLoginSuccessBlock:^{
//                    IKMOrderListPageVC *vc =[IKMOrderListPageVC initPageVC];
//                    vc.hidesBottomBarWhenPushed = YES;
//                    [self.navigationController pushViewController:vc animated:YES];
//                }];
//
//                UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:loginVC];
//                [self presentViewController:navi animated:YES completion:^{
//                }];
                
                void (^success)(void) = ^{
                    if ([UserDefaults boolForKey:@"isLogin"]) {
                        [BaseMediator basePresent:@"IKMOrderListPageVC" dic:@{@"hidesBottomBarWhenPushed":@1}];
                    }
                };
                [BaseMediator basePresent:@"IKLoginMainViewController" dic:@{@"loginSuccessBlock" : success}];
                

            }else{
                UIViewController *vc = [BaseMediator actionMethodFromClass:@"IKMOrderListPageVC" Selector:@"initPageVC" Prarms:nil];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
//                [BaseMediator basePresent:@"IKMOrderListPageVC" dic:@{@"hidesBottomBarWhenPushed":@1}];

            }
        }else if(indexPath.row == 2){
//            //健康管理
//            [MobClick event:@"mine_registered_order"];
//            [self goToWebVC:[IKHelpURLS getDefaultURLServiceH5]];
            [BaseMediator actionMethodFromClass:@"MobClick" Selector:@"event:" Prarms:@[@"mine_registered_order"]];
            
            NSString * urlPath = @"https://m.tijianbao.com/mine/sugarorder";
            NSDictionary * dic= [[NSUserDefaults standardUserDefaults] objectForKey:@"IKNetworkSettingHostURL"];
            if(dic && ([dic[@"HostModel"] integerValue]==4||[dic[@"HostModel"] integerValue]==3)) {
                urlPath = @"http://uat.m.tijianbao.com/mine/sugarorder";
            }
            
//            [BaseMediator basePresent:@"IKMainWebViewController" dic:@{@"hidesBottomBarWhenPushed":@1,@"url":urlPath}];
            [self goToWebVC:urlPath];
            
            
           
            
        }else if(indexPath.row == 3){
//            //挂号订单
//            [MobClick event:@"mine_registered_order"];
//            [self goToWebVC:[IKHelpURLS getDefaultURLRegisteredOrderH5]];
            [BaseMediator actionMethodFromClass:@"MobClick" Selector:@"event:" Prarms:@[@"mine_registered_order"]];

        
            
            NSString *webUrl = [NSString stringWithFormat:@"http://wx.daoyitong.com/#/myorder?pavoid=%@",[UserDefaults valueForKey:@"access_token"]?:@""];
            
            NSDictionary * dic= [[NSUserDefaults standardUserDefaults] objectForKey:@"IKNetworkSettingHostURL"];
            if(dic && ([dic[@"HostModel"] integerValue]==4||[dic[@"HostModel"] integerValue]==3)) {
                webUrl = [NSString stringWithFormat:@"http://uat.wx.daoyitong.com/#/myorder?pavoid=%@",[UserDefaults valueForKey:@"access_token"]?:@""];
            }
            [self goToWebVC:webUrl];
//            [BaseMediator basePresent:@"IKMainWebViewController" dic:@{@"hidesBottomBarWhenPushed":@1,@"url":webUrl}];
            
        }else if(indexPath.row == 4){
//            //咨询订单
//            [MobClick event:@"mine_advisory_order"];
//            [self goToWebVC:[IKHelpURLS getDefaultURLConsultingOrdersH5]];
            [BaseMediator actionMethodFromClass:@"MobClick" Selector:@"event:" Prarms:@[@"mine_advisory_order"]];

        
            NSString *webUrl = [NSString stringWithFormat:@"https://api.daoyitong.com/ForAppDyt/#/inquisitionOrder?pavoid=%@",[UserDefaults valueForKey:@"access_token"]?:@""];
            
            NSDictionary * dic= [[NSUserDefaults standardUserDefaults] objectForKey:@"IKNetworkSettingHostURL"];
            if(dic &&([dic[@"HostModel"] integerValue]==4||[dic[@"HostModel"] integerValue]==3)) {
                webUrl = [NSString stringWithFormat:@"https://api-uat.daoyitong.com/ForAppDyt/#/inquisitionOrder?pavoid=%@",[UserDefaults valueForKey:@"access_token"]?:@""];
            }
//            [BaseMediator basePresent:@"IKMainWebViewController" dic:@{@"hidesBottomBarWhenPushed":@1,@"url":webUrl}];
            [self goToWebVC:webUrl];

        }else if (indexPath.row == 5){
//            //邀请有奖
//            [MobClick event:@"mine_invite_prizes"];
//            IKMainWebViewController *vc = [[IKMainWebViewController alloc] init];
//            vc.hidesBottomBarWhenPushed = YES;
//            vc.url = [IKHelpURLS getInvitationActivity];
//            vc.isShare = YES;
//            vc.isInvitePrizes = YES;
//            vc.navTitle = @"邀请有奖";
//            [self.navigationController pushViewController:vc animated:YES];
            [BaseMediator actionMethodFromClass:@"MobClick" Selector:@"event:" Prarms:@[@"mine_invite_prizes"]];
            
            NSDictionary * dic= [[NSUserDefaults standardUserDefaults] objectForKey:@"IKNetworkSettingHostURL"];
            NSString *baseURL =dic ? dic[@"H5SetHostURL"] : @"http://m.ikang.com";
            NSString *webUrl = [NSString stringWithFormat:@"%@/#/invitationActivity",baseURL];
            
            [BaseMediator basePush:@"IKMainWebViewController" dic:@{@"hidesBottomBarWhenPushed":@1,@"url" :webUrl,@"isShare":@1,@"isInvitePrizes":@1,@"navTitle":@"邀请有奖"}];

            
        }else if (indexPath.row == 6){
//            //邀请有奖
//            [MobClick event:@"mine_about_tijianbao"];
//            IKAboutIKangController *vc =[IKAboutIKangController new];
//            vc.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:vc animated:YES];
            [BaseMediator actionMethodFromClass:@"MobClick" Selector:@"event:" Prarms:@[@"mine_about_tijianbao"]];

            [BaseMediator basePush:@"IKAboutIKangController" dic:@{@"hidesBottomBarWhenPushed":@1}];

        }
    }else if (indexPath.section == 2){
//        //人工客服
////        [MobClick event:@"mine_contact_manual_customer_service"];
        [BaseMediator actionMethodFromClass:@"MobClick" Selector:@"event:" Prarms:@[@"mine_contact_manual_customer_service"]];

        [self xiaoNengTalk];
    }
}
//跳转到优惠券
-(void)jumpToCoupon{
//    IKCouponViewController *vc = [[IKCouponViewController alloc] init];
//    vc.hidesBottomBarWhenPushed=YES;
//    [self.navigationController pushViewController:vc animated:YES];
    [BaseMediator basePush:@"IKCouponViewController" dic:@{@"hidesBottomBarWhenPushed":@1}];

}
- (void)goToWebVC:(NSString*)url{
    //挂号订单 / 咨询订单
    if (![UserDefaults boolForKey:@"isLogin"]){

        [BaseMediator basePresent:@"IKLoginMainViewController" dic:nil];

    }else{
        if([url containsString:@"/mine/"]) {
            [BaseMediator basePresent:@"IKHealthManagerWebController" dic:@{@"hidesBottomBarWhenPushed":@1,@"url":url}];

        }else {
            [BaseMediator basePresent:@"IKMainWebViewController" dic:@{@"hidesBottomBarWhenPushed":@1,@"url":url}];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * view = [[UIView alloc] init];
    view.backgroundColor = [UIColor baseBackgroundColor];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView * view = [[UIView alloc] init];
    view.backgroundColor = [UIColor baseBackgroundColor];
    return view;
}

-(NSMutableAttributedString *)getAttributedString:(NSString*)danwei count:(id)count{
    NSMutableAttributedString *couponStr = [[NSMutableAttributedString alloc]init];
    NSString *str0 =@"";
    if ([danwei isEqualToString:@"张"]) {
        str0 = [NSString stringWithFormat:@"%@ ",count];
    }
    if ([danwei isEqualToString:@"元"]) {
        str0 = [self positiveFormat:[NSString stringWithFormat:@"%.2f",[count floatValue]]];
    }
    NSDictionary *dictAttr0 = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:15],
                                NSForegroundColorAttributeName:[UIColor baseTextColor]};
    NSAttributedString *attr0 = [[NSAttributedString alloc]initWithString:str0 attributes:dictAttr0];
    [couponStr appendAttributedString:attr0];
    NSString *str1 = danwei;
    if ([danwei isEqualToString:@"元"]) {
        str1 = @"";
    }
    NSDictionary *dictAttr1 = @{NSForegroundColorAttributeName:[UIColor baseTextColor],
                                NSFontAttributeName:[UIFont systemFontOfSize:11]};
    NSAttributedString *attr1 = [[NSAttributedString alloc]initWithString:str1 attributes:dictAttr1];
    [couponStr appendAttributedString:attr1];
    return couponStr;
}
- (NSString *)positiveFormat:(NSString *)text{
    if(!text || [text floatValue] == 0){
        return @"0.00";
    }else{
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc]init];
        formatter.numberStyle = kCFNumberFormatterCurrencyStyle;
        formatter.locale = [NSLocale localeWithLocaleIdentifier:@"zh_CN"];
        return [formatter stringFromNumber: [NSNumber numberWithDouble:[text doubleValue]]];
    }
    return @"";
}

- (void)xiaoNengTalk{
    /**
     用户登录
     @param userId 登录用户ID：识别用户身份唯一识别，不能重复，即不能传固定值！否则会无法区分用户，造成串消息的。
     * 传值需要符合小能的传值规则 ：数字、英文字母和“@”、“.”、“_”三种字符。长度小于60。
     * 【登录用户必传，不能传空，也不能写死固定值】
     * 来源：集成方传入SDK。
     @param userName 登录用户名称:是显示到PC客服端的用户名称。
     * 传值需要符合小能的传值规则：字母、汉字、数字、_、@、.的字符串，长度小于120。
     * 若用户游客身份则传入空字符串，系统随机会生成一个用户名, 如:“游客AB321”。若登录用户，但是username传空值，则采用用户id作为username.
     * 来源：集成方传入，小能负责展示
     @return 参数判断的返回值,
     20005：登录成功；
     20006：重复登录，不需要再调用登录；
     20001：没有初始化，就调用登录方法，请检查下初始化和登陆的调用时机
     20002：userId为空,请检查传值；
     20003：userId非法，请检查传值是否符合规则；
     11002：3.1.4版本以前保持，3.1.4及以后版本废弃，表示userName不合法，请检查传值是否符合规则；
     [Ntalker ntalker_loginWithUserId:@"userId" andUserName:@"userName"];
     */
    
    /**
     初始化一个聊天窗口实体
     @param chatParams 创建聊天实体需要的参数：NtalkerChatParams
     @return 聊天窗口实体
     
     NtalkerChatParams *chatParems = [[NtalkerChatParams alloc] init];
     chatParems.settingId = @"";//接待组id【必填】，示例 "kf_20000_template_9"，
     chatParems.delegate = self;//聊天页面代理【选填】如果需要监听聊天页面事件或者自定义UI、扩展功能则必须设置代理。相应功能模块会提到具体方法。
     chatParems.productId = @"";//商品id 【选填】需要展示商品信息时填写，示例 @“ntalker_test”
     //创建聊天窗口
     UIViewController *chatVC = [Ntalker ntalker_chatViewControllerWithChatParam:chatParems];
     [self.navigationController pushViewController:chatVC animated:YES];
     */
    
    //TODO: 邵佳作更改 不用登录也能聊天
    //    if (![IKUserInfoDefault shareUserInfo].isLogin){
    //        IKLoginMainViewController *loginVC = [[IKLoginMainViewController alloc]initWithLoginSuccessBlock:^{
    //            NtalkerChatParams *chatParems = [[NtalkerChatParams alloc] init];
    //            chatParems.settingId = @"kf_20040_template_9999";//接待组id【必填】，示例 "kf_20000_template_9"，
    //            //chatParems.delegate = self;//聊天页面代理【选填】如果需要监听聊天页面事件或者自定义UI、扩展功能则必须设置代理。相应功能模块会提到具体方法。
    //            //chatParems.productId = @"";//商品id 【选填】需要展示商品信息时填写，示例 @“ntalker_test”
    //            //创建聊天窗口
    //            UIViewController *chatVC = [Ntalker ntalker_chatViewControllerWithChatParam:chatParems];
    //            chatVC.hidesBottomBarWhenPushed = YES;
    //            [self.navigationController pushViewController:chatVC animated:YES];
    //        }];
    //        UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:loginVC];
    //        [self presentViewController:navi animated:YES completion:^{
    //        }];
    //    }else{
//    NtalkerChatParams *chatParems = [[NtalkerChatParams alloc] init];
//    chatParems.settingId = @"kf_20040_template_9999";//接待组id【必填】，示例 "kf_20000_template_9"，
    //chatParems.delegate = self;//聊天页面代理【选填】如果需要监听聊天页面事件或者自定义UI、扩展功能则必须设置代理。相应功能模块会提到具体方法。
    //chatParems.productId = @"";//商品id 【选填】需要展示商品信息时填写，示例 @“ntalker_test”
    //创建聊天窗口
//    UIViewController *chatVC = [Ntalker ntalker_chatViewControllerWithChatParam:chatParems];
//    chatVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:chatVC animated:YES];
    //    }
    id instance =  [BaseMediator initClass:@"NtalkerChatParams" dic:@{@"settingId": @"kf_20040_template_9999"}];
    UIViewController *chatVC =[BaseMediator actionMethodFromClass:@"Ntalker" Selector:@"ntalker_chatViewControllerWithChatParam" Prarms:instance];
    chatVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatVC animated:YES];

    
}


- (UIButton*)idfaButton{
    if (!_idfaButton) {
        //WS(ws)
        _idfaButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
        _idfaButton.backgroundColor = [UIColor clearColor];
        _idfaButton.titleLabel.font = Font(14);
        [_idfaButton setTitle:[NSString getIDFAFromKeychain] forState:UIControlStateNormal];
        [_idfaButton setTitleColor:[UIColor baseTextColor] forState:UIControlStateNormal];
        [_idfaButton addActionforControlEvents:UIControlEventTouchUpInside Completion:^{
            [EasyShow showText:@"复制成功"];
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = [NSString getIDFAFromKeychain];
        }];
    }
    return _idfaButton;
}

-(NSArray*)dataArray{
    if (!_dataArray) {
        self.dataArray = @[
                           @[
                               @{@"title":@"优惠券",@"icon":@"profile_ic_coupon",@"viewController":@"IKCouponViewController"},
                               ],
                           @[
                               @{@"title":@"我的报告",@"icon":@"profile_ic_baogao",@"viewController":@""},
                               @{@"title":@"我的订单",@"icon":@"profile_ic_dingdan",@"viewController":@"IKAboutIKangController"},
                               @{@"title":@"血糖管理",@"icon":@"profile_ic_service",@"viewController":@"IKAboutIKangController"},
                               @{@"title":@"挂号订单",@"icon":@"profile_ic_outpatient",@"viewController":@"IKAboutIKangController"},
                               @{@"title":@"咨询订单",@"icon":@"profile_ic_Consultation",@"viewController":@"IKAboutIKangController"},
                               @{@"title":@"邀请有奖",@"icon":@"profile_ic_prize",@"viewController":@"IKAboutIKangController"},
                               @{@"title":@"关于体检宝",@"icon":@"profile_ic_ik",@"viewController":@"IKAboutIKangController"},
                               ],
                           @[
                               @{@"title":@"联系人工客服",@"icon":@"",@"":@""},
                               ],
                           ];
    }
    return  _dataArray;
}
- (IKMUserMePresenter*)presenter{
    if (!_presenter) {
        _presenter = [[IKMUserMePresenter alloc]init];
    }
    return _presenter;
}

@end
