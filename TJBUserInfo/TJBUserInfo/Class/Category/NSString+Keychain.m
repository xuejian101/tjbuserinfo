//
//  NSString+Keychain.m
//  TJBLeaf
//
//  Created by iOS123 on 2018/12/19.
//  Copyright © 2018 ikang. All rights reserved.
//

#import "NSString+Keychain.h"
#import <AdSupport/AdSupport.h>
#import "SFHFKeychainUtils.h"

static NSString * const kDeviceIdentifier = @"kDeviceIdentifier";

@implementation NSString (Keychain)

+ (NSString *)getIDFAFromKeychain{
    //从钥匙串中获取唯一设备标识
    NSString * deviceIdentifier = [SFHFKeychainUtils getPasswordForUsername:kDeviceIdentifier andServiceName:[[NSBundle mainBundle] bundleIdentifier] error:nil];
    if (deviceIdentifier) {
        //如果钥匙串中存在唯一标识，则直接返回
        return deviceIdentifier;
    }
    //获取IDFA
    NSString *IDFA = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    //判断IDFA是否为空
    BOOL isEmpty = [[IDFA stringByReplacingOccurrencesOfString:@"-" withString:@""] stringByReplacingOccurrencesOfString:@"0" withString:@""].length;
    if (isEmpty) {
        //不为空，将IDFA作为唯一标识
        deviceIdentifier = IDFA;
    }
    else {
        //为空，获取UUID作为唯一标识
        deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    }
    //保存唯一设备标识,如已存在则不进行任何处理
    [SFHFKeychainUtils storeUsername:kDeviceIdentifier andPassword:deviceIdentifier forServiceName:[[NSBundle mainBundle]bundleIdentifier] updateExisting:NO error:nil];
    //返回唯一标识
    return deviceIdentifier;
}

@end
