//
//  NSString+Keychain.h
//  TJBLeaf
//
//  Created by iOS123 on 2018/12/19.
//  Copyright © 2018 ikang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Keychain)
+ (NSString *)getIDFAFromKeychain;
@end

NS_ASSUME_NONNULL_END
