//
//  IKMUserMePresenter.h
//  TJBUserInfo
//
//  Created by 薛健 on 2019/7/2.
//  Copyright © 2019 iKang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IKSettingModel.h"
typedef void (^UsermeTotalMoneyAndNums)(IKCardNumModel * _Nullable dataModel);

typedef void (^UsermeNewHomeMessage)(IKHomeNewMessageModel * _Nullable dataModel);

NS_ASSUME_NONNULL_BEGIN

@interface IKMUserMePresenter : NSObject

-(void)getTotalMoneyAndNumsBlock:(UsermeTotalMoneyAndNums)block;

-(void)getNewHomeMessageBlock:(UsermeNewHomeMessage)block;

@end

NS_ASSUME_NONNULL_END
