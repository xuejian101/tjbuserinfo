//
//  IKMUserMePresenter.m
//  TJBUserInfo
//
//  Created by 薛健 on 2019/7/2.
//  Copyright © 2019 iKang. All rights reserved.
//

#import "IKMUserMePresenter.h"
#import <EasyShow.h>
@interface IKMUserMePresenter()

@end
@implementation IKMUserMePresenter
-(void)getTotalMoneyAndNumsBlock:(UsermeTotalMoneyAndNums)block
{
    NSString * apiName = [NSString stringWithFormat:@"storedValueCard/totalMoneyAndNums"];
    [IKMBaseRequest request:apiName
                 parameters:nil
                   hostType:IKMTypeOfApp
                requestType:IKMRequestType_Get
                 modelClass:nil
                    success:^(id response) {
                        NSDictionary *respondInfo = (NSDictionary *)response;
                        NSLog(@"%@",respondInfo);
                        NSArray *array = respondInfo[@"results"];
                        if ([respondInfo[@"code"] intValue] == 1  && array.count > 0){
                            IKMyCardModel * myCardModel = [[IKMyCardModel alloc] initWithDictionary:respondInfo error:nil];
                            NSLog(@"%@",myCardModel);
                            IKCardNumModel *model = myCardModel.results[0];
                            NSLog(@"%@",model);

                            block(model);
                        }
                    } failure:^(NSError *error) {
                        NSDictionary *dic = error.userInfo;
                        if(dic){
                            [EasyShow showText:dic[@"message"]];
                        }else{
                            [EasyShow showText:@"请求失败"];
                        }
                        [EasyShow hidenLoading];
                    }];
}

-(void)getNewHomeMessageBlock:(UsermeNewHomeMessage)block
{
    NSString * apiName = [NSString stringWithFormat:@"v2/main/remind"];
    [IKMBaseRequest request:apiName
                 parameters:nil
                   hostType:IKMTypeOfApp
                requestType:IKMRequestType_Get
                 modelClass:nil
                    success:^(id response) {
                        NSDictionary *respondInfo = (NSDictionary *)response;
                        NSArray *array = respondInfo[@"results"];
                        if ([respondInfo[@"code"] intValue] == 1  && array.count > 0){
                            IKHomeNewMessageInfoModel * homeMessageModel = [[IKHomeNewMessageInfoModel alloc] initWithDictionary:respondInfo error:nil];
                            NSLog(@"%@",homeMessageModel);
                            IKHomeNewMessageModel *model = homeMessageModel.results[0];
                            NSLog(@"%@",model);
                            block(model);
                        }
                    } failure:^(NSError *error) {
                        NSDictionary *dic = error.userInfo;
                        if(dic){
                            [EasyShow showText:dic[@"message"]];
                        }else{
                            [EasyShow showText:@"请求失败"];
                        }
                        [EasyShow hidenLoading];
                    }];
}


@end
