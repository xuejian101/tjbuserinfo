//
//  IKSettingModel.h
//  TJBUserInfo
//
//  Created by 薛健 on 2019/7/3.
//  Copyright © 2019 iKang. All rights reserved.
//

#import <IKCoreModule/IKCoreModule.h>


@protocol IKCardNumModel @end
@interface IKCardNumModel : IKMBaseModel

@property(nonatomic,strong) NSString *couponsNum;
@property(nonatomic,strong) NSNumber *totalMoney;
@end

@interface IKMyCardModel : IKMBaseModel
@property(nonatomic,strong) NSNumber *code;
@property(nonatomic,strong) NSString *message;
@property(nonatomic,strong) NSString *errors;
@property(nonatomic,strong) NSArray <IKCardNumModel> *results;
@end

@protocol IKHomeNewMessageModel @end
@interface IKHomeNewMessageModel : IKMBaseModel
@property(nonatomic,strong) NSString *dentalTime;
@property(nonatomic,strong) NSString *medicalTime;
@property(nonatomic,strong) NSString *messageCount;
@property(nonatomic,strong) NSString *reportRemind;

@end
@interface IKHomeNewMessageInfoModel : IKMBaseModel
@property(nonatomic,assign) NSInteger code;
@property(nonatomic,strong) NSString *message;
@property(nonatomic,strong) NSString *errors;
@property(nonatomic,strong) NSArray <IKHomeNewMessageModel> *results;
@end


