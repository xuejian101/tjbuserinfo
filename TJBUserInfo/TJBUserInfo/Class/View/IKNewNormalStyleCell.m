//
//  IKNewNormalStyleCell.m
//  TJBLeaf
//
//  Created by iOS123 on 2018/9/21.
//  Copyright © 2018年 ikang. All rights reserved.
//

#import "IKNewNormalStyleCell.h"
#import <Masonry.h>
#import <IKToolsModule.h>
@implementation IKNewNormalStyleCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        WS(ws);
        _iconView = [[UIImageView alloc]init];
        _iconView.image = [UIImage imageNamed:@""];
        [self.contentView addSubview:_iconView];
        [_iconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.left.mas_equalTo(15);
            make.width.height.mas_equalTo(23);
        }];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor = [UIColor baseTextColor];
        _titleLabel.font =Font(15);
        _titleLabel.numberOfLines = 2;
        [self.contentView addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.left.mas_equalTo(ws.iconView.mas_right).offset(20);
            make.right.mas_equalTo(-70);
        }];
        
        _arrowView = [[UIImageView alloc]init];
        _arrowView.image = [UIImage imageNamed:@""];
        [self.contentView addSubview:_arrowView];
        [_arrowView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.right.mas_equalTo(-15);
            make.width.mas_equalTo(8);
            make.height.mas_equalTo(14);
        }];
    }
    return self;
}

@end
