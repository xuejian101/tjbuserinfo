//
//  IKAccountControllerCell.h
//  TJBLeaf
//
//  Created by iOS123 on 2019/1/11.
//  Copyright © 2019 ikang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IKAccountControllerCell : UITableViewCell
@property (strong, nonatomic) UIImageView *iconView;
@property (strong, nonatomic) UIImageView *arrowView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *detailLabel;
@end

NS_ASSUME_NONNULL_END
