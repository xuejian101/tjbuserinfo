//
//  IKNewNormalStyleCell.h
//  TJBLeaf
//
//  Created by iOS123 on 2018/9/21.
//  Copyright © 2018年 ikang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKNewNormalStyleCell : UITableViewCell
@property (strong, nonatomic)  UIImageView *iconView;
@property (strong, nonatomic)  UIImageView *arrowView;
@property (strong, nonatomic)  UILabel *titleLabel;
@property (strong, nonatomic)  UILabel *detailLabel;
@end


