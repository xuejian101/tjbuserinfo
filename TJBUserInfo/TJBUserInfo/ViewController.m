//
//  ViewController.m
//  TJBUserInfo
//
//  Created by 薛健 on 2019/7/1.
//  Copyright © 2019 iKang. All rights reserved.
//

#import "ViewController.h"
#import "IKMNetworkConfig.h"
#import "IKToolsModule.h"
#import "UIImage+Loader.h"
#import "TJBUserInfoViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[IKMNetworkConfig shareConfig] configWithTarget:self frame:CGRectMake(100, 300, 100, 50) exitApp:NO complete:^{
        NSLog(@"%@",NetworkHost);
    }];
    WS(weakSelf);
    [UIButton masButtonWithTitle:@"跳转" titleColor:[UIColor baseColor] backColor:[UIColor grayColor] fontSize:16 cornerRadius:5 supView:self.view constraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
        make.width.equalTo(@(200));
        make.height.equalTo(@(60));
    } touchUp:^(id sender) {
        [weakSelf goJump];
    }];
    // Do any additional setup after loading the view.
}
-(void)goJump
{
    NSLog(@"---跳转---");
    TJBUserInfoViewController *vc = [[TJBUserInfoViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil]; 
}

@end
