//
//  IKMBaseRequest.m
//  IKCoreModule
//
//  Created by HU on 2018/6/4.
//  Copyright © 2018年 HU. All rights reserved.
//


#import "IKMBaseRequest.h"
#import "IKMBaseModel.h"
/*****************************APP线上环境地质*******************************/

////////爱康APP环境
NSString *const IKMAPPHostURL = @"https://ikapp.health.ikang.com";
////////体检宝APP环境
NSString *const TJBAPPHostURL = @"https://tjbappweb.health.ikang.com";
////////统一用户环境
NSString *const UnitUserHostURL = @"https://oauth2.health.ikang.com";

/************************************************************************/
/**
 *  统一用户秘钥
 */
static NSString *const Client_Name = @"iKang_app_iOS";
////////爱康APP
NSString *const IK_Client_Id = @"9c92eba0-e5f3-4078-a28d-f360c1557663";
static NSString *const IK_Client_Secret = @"595a166f-0ea0-4cfe-9afb-1d82b9203597";
////////体检宝APP
NSString *const TJB_Client_Id = @"e3d4b0d0-6cde-41f1-94b0-d06cdea1f188";
static NSString *const TJB_Client_Secret = @"837bb114-b497-4c11-bbb1-14b497ec115f";

///此处方法配合JHNetworkConfig使用,无害化嵌入环境
static NSString *const urlNetwork = @"IKNetworkSettingHostURL";
/************************************************************************/

static NSString *const UserLoginError = @"UserLoginError";

static NSInteger cycleIndex = 0;
@implementation IKMBaseRequest

+(void)initialize{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ //只执行一次就可以了
        //初次请求加载cookie
        if ([[NSUserDefaults standardUserDefaults] valueForKey:@"access_token"]) {
            [self loadCookies];
        }
    });
}
#pragma mark - 请求的公共方法
/**
 获取完整URL
 @param name         api名称
 @return     URL
 */
+(NSString *)getURLWithName:(NSString *)name
                   hostType:(IKMHostType)hostType{
    ///此处方法配合JHNetworkConfig使用,无害化嵌入环境
    NSDictionary *urlDic =  [[NSUserDefaults standardUserDefaults] objectForKey:urlNetwork];
    switch (hostType) {
        case IKMTypeOfUnitUser:{
            [IKMNetworking setRequestSerializer:IKMRequestSerializerHTTP];
            //请求的头
            NSString *secretStr = @"";
            if ([[[NSBundle mainBundle]bundleIdentifier] isEqualToString:@"com.ikang.app"]) {/////////体检宝
                secretStr = [NSString stringWithFormat:@"%@:%@",TJB_Client_Id,TJB_Client_Secret];
            }else{//.//////爱康
                secretStr = [NSString stringWithFormat:@"%@:%@",IK_Client_Id,IK_Client_Secret];
            }
            secretStr = [self base64String:secretStr];
            secretStr = [NSString stringWithFormat:@"Basic %@",secretStr];
            [IKMNetworking setValue:secretStr forHTTPHeaderField:@"Authorization"];
            
            NSString *hostURL = nil;
#ifdef DEBUG
            hostURL = urlDic[@"UnitUserHostURL"];
#endif
            NSString *requestUrl =[NSString stringWithFormat:@"%@/%@",hostURL.length>0? hostURL :UnitUserHostURL, name];
            return requestUrl;
            break;}
            
        default:{
            [IKMNetworking setRequestSerializer:IKMRequestSerializerJSON];
            [IKMNetworking setValue:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forHTTPHeaderField:@"appVersion"];
            [IKMNetworking setValue:[NSString stringWithFormat:@"%.0f",[[NSDate date] timeIntervalSince1970]*1000] forHTTPHeaderField:@"time"];
            [IKMNetworking setValue:[NSString stringWithFormat:@"%f",[[[UIDevice currentDevice] systemVersion] floatValue]] forHTTPHeaderField:@"systemVersion"];
            [IKMNetworking setValue:@"iOS" forHTTPHeaderField:@"clientType"];
            [IKMNetworking setValue:@"37" forHTTPHeaderField:@"comefrom"];
            [IKMNetworking setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"access_token"]?:@"" forHTTPHeaderField:@"token"];
            //            [IKMNetworking setValue:[NSString stringWithFormat:@"%fX%f",SCREEN_WIDTH,SCREEN_HEIGHT] forHTTPHeaderField:@"screenSize"];

#ifdef DEBUG
            NSString *hostURL = urlDic[@"APPHostURL"];
            NSString *pathURL = urlDic[@"APPPathURL"];
            NSString *requestUrl =[NSString stringWithFormat:@"%@/%@/%@",hostURL.length>0? hostURL :IKMAPPHostURL, pathURL.length>0 ? pathURL :@"appService" ,name];
            return requestUrl;
#else
            if ([[[NSBundle mainBundle]bundleIdentifier] isEqualToString:@"com.ikang.app"]) {/////////体检宝
                NSString *requestUrl =[NSString stringWithFormat:@"%@/api/%@",TJBAPPHostURL, name];
                return requestUrl;
            }else{//.//////爱康
                NSString *requestUrl =[NSString stringWithFormat:@"%@/appService/%@",IKMAPPHostURL, name];
                return requestUrl;
            }
#endif
            break;}
    }

}
/**
 请求
 @param apiName     api名字
 @param parameter   传递参数
 @param modelClass model类名称,不传则返回解析后的字典
 @param success  请求成功回调
 @param failure 请求失败回调
 @return     ...
 */
+ (NSURLSessionTask *)request:(NSString *)apiName
                   parameters:(id)parameter
                     hostType:(IKMHostType)hostType
                  requestType:(IKMRequestType)requestType
                   modelClass:(NSString *)modelClass
                      success:(IKMRequestSuccess)success
                      failure:(IKMRequestFailure)failure{
    return [IKMNetworking request:[self getURLWithName:apiName hostType:hostType] requestType:requestType parameters:parameter success:^(id responseObject) {
        NSDictionary *responseDict = (NSDictionary *)responseObject;

        NSString *codeStr =[responseDict objectForKey:@"code"];
        NSNumber * code = [NSNumber numberWithInteger:codeStr.integerValue];

        if (hostType == IKMTypeOfUnitUser) {
            [self cookTheResponse:responseObject
                       modelClass:NSClassFromString(modelClass)
                         hostType:hostType
                          success:success
                          failure:failure];

        }else{
            if ([code isEqualToNumber:@1]) {
                cycleIndex = 0;
                [self cookTheResponse:responseObject
                           modelClass:NSClassFromString(modelClass)
                             hostType:hostType
                              success:success
                              failure:failure];
            }else{
                //登录超时 或者异地登陆
                if ([code isEqualToNumber:@2]) {
                    cycleIndex +=1;
                    if (cycleIndex<3) {
                        [self loadLoginCookie:^(id response) {
                            [self request:apiName parameters:parameter hostType:hostType requestType:requestType modelClass:modelClass success:success failure:failure];
                        } failure:^(NSError *error) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:UserLoginError object:responseDict];
                        }];
                    }else{
                        cycleIndex = 0;
                        [[NSNotificationCenter defaultCenter] postNotificationName:UserLoginError object:responseDict];
                    }
                    
                }else if ([code isEqualToNumber:@3]) {
                    cycleIndex = 0;
                    [[NSNotificationCenter defaultCenter] postNotificationName:UserLoginError object:responseDict];
                }else{
                    cycleIndex = 0;
                    [self cookTheResponse:responseObject
                               modelClass:NSClassFromString(modelClass)
                                 hostType:hostType
                                  success:success
                                  failure:failure];
                }
            }
        }
    } failure:^(NSError *error) {
        cycleIndex = 0;
        failure(error);
    }];
}
/**
 *  上传单/多张图片
 *
 *  @param apiName        请求地址
 *  @param parameters 请求参数
 *  @param imageDatas     图片数组
 *  @param success    请求成功的回调
 *  @param failure    请求失败的回调
 *
 *  @return 返回的对象可取消请求,调用cancel方法
 */
+(__kindof NSURLSessionTask *)uploadImages:(NSString *)apiName
                                parameters:(id)parameters
                                ImageDatas:(NSArray *)imageDatas
                                   success:(IKMHttpRequestSuccess)success
                                   failure:(IKMHttpRequestFailed)failure{
    return [IKMNetworking uploadImagesWithURL:[self getURLWithName:apiName hostType:IKMTypeOfApp] parameters:parameters ImageDatas:imageDatas success:success failure:failure];
}
/**
 *  上传单张图片
 *
 *  @param apiName        请求地址
 *  @param parameters 请求参数
 *  @param image     图片
 *  @param success    请求成功的回调
 *  @param failure    请求失败的回调
 *
 *  @return 返回的对象可取消请求,调用cancel方法
 */
+ (NSURLSessionTask *)uploadImage:(NSString *)apiName
                       parameters:(id)parameters
                        withImage:(UIImage *)image
                          success:(IKMHttpRequestSuccess)success
                          failure:(IKMHttpRequestFailed)failure{
    return [IKMNetworking uploadImageWithURL:[self getURLWithName:apiName hostType:IKMTypeOfApp] parameters:parameters withImage:image success:success failure:failure];
}


/**
 *  下载文件
 *
 *  @param apiName      请求地址
 *  @param fileDir  文件存储目录(默认存储目录为Download)
 *  @param progress 文件下载的进度信息
 *  @param success  下载成功的回调(回调参数filePath:文件的路径)
 *  @param failure  下载失败的回调
 *
 *  @return 返回NSURLSessionDownloadTask实例，可用于暂停继续，暂停调用suspend方法，开始下载调用resume方法
 */
+ (__kindof NSURLSessionTask *)download:(NSString *)apiName
                                fileDir:(NSString *)fileDir
                               progress:(IKMHttpProgress)progress
                                success:(void(^)(NSString *filePath))success
                                failure:(IKMHttpRequestFailed)failure{
    return [IKMNetworking downloadWithURL:[self getURLWithName:apiName hostType:IKMTypeOfApp] fileDir:fileDir progress:progress success:success failure:failure];
}
/**
 *  处理f请求到的报文
 */
+(void)cookTheResponse:(id)responseObject
            modelClass:(Class)modelClass
              hostType:(IKMHostType)hostType
               success:(IKMRequestSuccess)success
               failure:(IKMRequestFailure)failure{
    if([responseObject isKindOfClass:[NSDictionary class]]){
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        if (hostType == IKMTypeOfUnitUser) {
            NSString *errorCodeStr =[responseDict objectForKey:@"error_code"];
            if (errorCodeStr.length>0 && [errorCodeStr isEqualToString:@"0"]) {
                [self sendSuccRequest:responseDict
                           modelClass:modelClass
                              success:success];
            }else{
                [self sendErrorRequest:responseDict
                             errorCode:[responseDict[@"error_code"] integerValue]
                              errorMsg:responseDict[@"error_msg"]
                               failure:failure];
            }
        }else{
            NSInteger code = [responseDict[@"code"] integerValue];
            if (code!= 1) {
                [self sendErrorRequest:responseDict
                             errorCode:code
                              errorMsg:responseDict[@"message"]
                               failure:failure];
            }else{
                [self sendSuccRequest:responseDict
                           modelClass:modelClass
                              success:success];
            }
        }
    }
}
/**
 *  处理f请求到的成功报文
 */
+(void)sendSuccRequest:(NSDictionary *)responseDict
            modelClass:(Class)modelClass
               success:(IKMRequestSuccess)success{
    if (modelClass) {
        NSError * err = nil;
        IKMBaseModel * resultModel = [[modelClass alloc] initWithDictionary:responseDict error:&err];
        success(resultModel);
    } else{
        success(responseDict);
    }
}
/**
 *  处理f请求到的失败报文
 */
+(void)sendErrorRequest:(NSDictionary *)responseDict
              errorCode:(NSInteger)errorCode
              errorMsg:(NSString *)errorMsg
                failure:(IKMRequestFailure)failure{
    NSError *error = [NSError errorWithDomain:errorMsg?:@"" code:errorCode userInfo:responseDict];
    failure(error);
}
/**
 *  登录sesisson
 */
+(void)loadLoginCookie:(IKMRequestSuccess)success
               failure:(IKMRequestFailure)failure{
    //清空所有Cookie
    [self cleanCookies];
    
    NSMutableString * apiName = [NSMutableString stringWithString:@"login?deviceType=ios&comefrom=37"];
    
    NSString *tmpToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"access_token"];
    NSString *tmpMobile = [[NSUserDefaults standardUserDefaults] valueForKey:@"mobile"];
    
    if (tmpToken){
        [apiName appendFormat:@"&token=%@",tmpToken];
    }
    if (tmpMobile) {
        [apiName appendFormat:@"&telNo=%@",tmpMobile];
    }
    
    [IKMNetworking request:[self getURLWithName:apiName hostType:IKMTypeOfApp]
               requestType:IKMRequestType_Get
                parameters:nil
                   success:^(id responseObject) {
                       [self saveCookies];
                       success(responseObject);
                   }
                   failure:failure];
}

+ (void)saveCookies{
    NSData *cookiesData = [NSKeyedArchiver archivedDataWithRootObject: [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject: cookiesData forKey: @"org.ikang.cookie"];
    [defaults synchronize];
}

+ (void)loadCookies{
    NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey: @"org.ikang.cookie"]];
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in cookies){
        [cookieStorage setCookie: cookie];
    }
}

+ (void)cleanCookies{
    //清空所有Cookie
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *cookieArray = [NSArray arrayWithArray:[cookieJar cookies]];
    for (id obj in cookieArray) {
        [cookieJar deleteCookie:obj];
    }
}


+ (NSString *)base64String:(NSString *)str{
    NSData *theData = [str dataUsingEncoding: NSASCIIStringEncoding];
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}
@end
