//
//  IKMBaseRequest.h
//  IKCoreModule
//
//  Created by HU on 2018/6/4.
//  Copyright © 2018年 HU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IKMNetworking.h"
/*****************************APP线上环境地质*******************************/
////////爱康APP环境
UIKIT_EXTERN NSString *const IKMAPPHostURL;
////////体检宝APP环境
UIKIT_EXTERN NSString *const TJBAPPHostURL;
////////统一用户环境
UIKIT_EXTERN NSString *const UnitUserHostURL;
/*****************************APP线上环境地质*******************************/
////////爱康APP 客户端ID
UIKIT_EXTERN NSString *const IK_Client_Id;
////////体检宝APP 客户端ID
UIKIT_EXTERN NSString *const TJB_Client_Id;
/**
 
 这是对AFNetworking的第二层隔离封装,简单化封装各种网络请求方法,并且统一处理回调逻辑
 
 */
/**
 请求成功的block
 @param response 响应体数据 当传需要解析的model时返回model model为nil则表示不需要解析,返回字典
 */
typedef void(^IKMRequestSuccess)(id response);
/**
 请求失败的block
 @param error 扩展信息
 */
typedef void(^IKMRequestFailure)(NSError *error);

@interface IKMBaseRequest : NSObject

/**
 获取完整URL
 @param name         api名称
 @return     URL
 */
+(NSString *)getURLWithName:(NSString *)name
                   hostType:(IKMHostType)hostType;
/**
 请求
 @param apiName     api名字
 @param parameter   传递参数
 @param modelClass model类名称,不传则返回解析后的字典
 @param success  请求成功回调
 @param failure 请求失败回调
 @return     ...
 */
+ (NSURLSessionTask *)request:(NSString *)apiName
                   parameters:(id)parameter
                     hostType:(IKMHostType)hostType
                  requestType:(IKMRequestType)requestType
                   modelClass:(NSString *)modelClass
                      success:(IKMRequestSuccess)success
                      failure:(IKMRequestFailure)failure;
/**
 *  上传单/多张图片
 *
 *  @param apiName        请求地址
 *  @param parameters 请求参数
 *  @param imageDatas     图片数组
 *  @param success    请求成功的回调
 *  @param failure    请求失败的回调
 *
 *  @return 返回的对象可取消请求,调用cancel方法
 */
+(__kindof NSURLSessionTask *)uploadImages:(NSString *)apiName
                                parameters:(id)parameters
                                ImageDatas:(NSArray *)imageDatas
                                   success:(IKMHttpRequestSuccess)success
                                   failure:(IKMHttpRequestFailed)failure;
/**
 *  上传单张图片
 *
 *  @param apiName        请求地址
 *  @param parameters 请求参数
 *  @param image     图片
 *  @param success    请求成功的回调
 *  @param failure    请求失败的回调
 *
 *  @return 返回的对象可取消请求,调用cancel方法
 */
+ (NSURLSessionTask *)uploadImage:(NSString *)apiName
                       parameters:(id)parameters
                        withImage:(UIImage *)image
                          success:(IKMHttpRequestSuccess)success
                          failure:(IKMHttpRequestFailed)failure;


/**
 *  下载文件
 *
 *  @param apiName      请求地址
 *  @param fileDir  文件存储目录(默认存储目录为Download)
 *  @param progress 文件下载的进度信息
 *  @param success  下载成功的回调(回调参数filePath:文件的路径)
 *  @param failure  下载失败的回调
 *
 *  @return 返回NSURLSessionDownloadTask实例，可用于暂停继续，暂停调用suspend方法，开始下载调用resume方法
 */
+ (__kindof NSURLSessionTask *)download:(NSString *)apiName
                                fileDir:(NSString *)fileDir
                               progress:(IKMHttpProgress)progress
                                success:(void(^)(NSString *filePath))success
                                failure:(IKMHttpRequestFailed)failure;

+(void)loadLoginCookie:(IKMRequestSuccess)success
               failure:(IKMRequestFailure)failure;
@end
