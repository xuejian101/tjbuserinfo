//
//  IKCoreModule.h
//  IKCoreModule
//
//  Created by 狄烨 . on 2019/1/10.
//  Copyright © 2019 HU. All rights reserved.
//

#ifndef IKCoreModule_h
#define IKCoreModule_h

#define Request_Fail            @"请求失败，请稍后重试"               ///请求失败
#define Request_Back_Error      @"服务器返回数据异常，请重试"          ///请求成功但是error
#define Request_Session_Timeout @"登录信息已过期，请重新登录"          ///3次请求session login错误提示
#define Network_Request_Fail    @"网络不给力"                       ///网络连接失败或超时

#import "IKMBaseRequest.h"
#import "IKMBaseModel.h"

#endif /* IKCoreModule_h */
