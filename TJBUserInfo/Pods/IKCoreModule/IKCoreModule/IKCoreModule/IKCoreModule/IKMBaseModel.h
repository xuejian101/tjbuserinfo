//
//  IKMBaseModel.h
//  IKCoreModule
//
//  Created by HU on 2018/6/4.
//  Copyright © 2018年 HU. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface IKMBaseModel : JSONModel
///将model存储,子类去实现
+ (void)saveWithDic:(NSDictionary *)dic;
///删除存储,子类去实现
+ (void)remove;
//读取存储,子类去实现
+ (__kindof IKMBaseModel *)getCurrentSaveModel;
@end
