//
//  EasyShow.m
//  IKToolsModule
//
//  Created by HU on 2018/8/2.
//  Copyright © 2018年 HU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import "EasyShow.h"
#import "EasyShowConfig.h"
#import "IKMToastView.h"
#import <IKMediator/UIApplication+GetRootVC.h>

@implementation EasyShow
+(void)initialize{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ //只执行一次就可以了
        EasyShowConfig *options = [EasyShowConfig shared];
        options.loadingBgColor = [UIColor clearColor];
        options.loadingShadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        NSMutableArray *tempArr = [NSMutableArray array];
        if ([[[NSBundle mainBundle]bundleIdentifier] isEqualToString:@"com.ikang.official"]) {/////////爱康
            options.loadingTextFont = [UIFont systemFontOfSize:13];
            options.loadingTextColor = ColorFromName(0x333333);
            options.loadingSpace = 6;
            options.loadingHorizontalPadding = 18;
            options.loadingAnimationTime = 1.0;
            for (int i = 0; i < 12; i++) {
                UIImage *img = [self loadShowImageNamed:[NSString stringWithFormat:@"IKloading_%d",i]];
                [tempArr addObject:img] ;
            }
        }else if ([[[NSBundle mainBundle]bundleIdentifier] isEqualToString:@"com.ikang.app"]){//.//////体检宝
            options.loadingAnimationTime = 0.5;
            for (int i = 0; i < 15; i++) {
                UIImage *img = [self loadShowImageNamed:[NSString stringWithFormat:@"loading_%d",i]];
                [tempArr addObject:img] ;
            }
        }
        if (tempArr.count>0) {
            options.loadingImagesArray = tempArr;
        }else{
            options.loadingTintColor = [UIColor colorWithWhite:0 alpha:0.9];
            options.activityColor = [UIColor whiteColor];
        }
        
        options.alertRightColor = ColorFromName(0xea5504);
    });
}

+ (UIImage *)loadShowImageNamed:(NSString *)name{
    NSString *mainBundlePath = [NSBundle mainBundle].bundlePath;
    NSString *bundlePath = [NSString stringWithFormat:@"%@/%@",mainBundlePath,@"IKEasyShowView.bundle"];
    NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
    if (bundle == nil) {
        bundlePath = [NSString stringWithFormat:@"%@/%@",mainBundlePath,@"Frameworks/IKEasyShowView.framework/IKEasyShowView.bundle"];
        bundle = [NSBundle bundleWithPath:bundlePath];
    }
    if ([UIImage respondsToSelector:@selector(imageNamed:inBundle:compatibleWithTraitCollection:)]) {
        return [UIImage imageNamed:name inBundle:bundle compatibleWithTraitCollection:nil];
    } else {
        return [UIImage imageWithContentsOfFile:[bundle pathForResource:name ofType:nil]];
    }
}

#pragma mark-Toast
/** 纯文本toast提示 */
+ (void)showText:(NSString *)text{
    [self showText:text withImage:nil];
}
/** 图文toast提示 */
+ (void)showText:(NSString *)text withImage:(UIImage *)image{
    
    //显示之前隐藏还在显示的视图
    NSEnumerator *subviewsEnum = [[[UIApplication sharedApplication] mainWindow].subviews reverseObjectEnumerator];
    for (UIView *subview in subviewsEnum) {
        if ([subview isKindOfClass:[IKMToastView class]]) {
            IKMToastView *showView = (IKMToastView *)subview ;
            [showView removeFromSuperview];
        }
    }
    // 背景view
    IKMToastView *toastView = [[IKMToastView alloc] init];
    toastView.text = text;
    toastView.image = image;
    [[[UIApplication sharedApplication] mainWindow] addSubview:toastView];
    [toastView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(toastView.superview);
    }];
    
    // 2秒后移除toast
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)([EasyShowConfig shared].toastShowTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.5 animations:^{
            toastView.alpha = 0;
        } completion:^(BOOL finished) {
            [toastView removeFromSuperview];
        }];
    });
}

#pragma mark - loading
/**
 可控制用户交互并且带说明信息的loading图
 @param text 说明信息
 @param view 展示位置
 @param isEnable 是否允许用户交互
 */
+ (IKMLoadingView *)showLoadingText:(NSString *)text onView:(UIView *)view enableEvent:(BOOL)isEnable{
    IKMLoadingView *loadingView = [IKMLoadingView new];
    if (view) {
        [self hidenLoadingOnView:view];
        [view addSubview:loadingView];
        [view bringSubviewToFront:loadingView];
        loadingView.layer.zPosition = MAXFLOAT;
    }else{
        [self hidenLoading];
        [[[UIApplication sharedApplication] mainWindow] addSubview:loadingView];
    }
    loadingView.text = text;
    [loadingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    loadingView.userInteractionEnabled = !isEnable;
    return loadingView;
}

/**
 loading图展示在UIview上
 */
+ (IKMLoadingView *)showLoadingText:(NSString *)text onView:(UIView*)view{
    return [self showLoadingText:text onView:view enableEvent:NO];
}

+ (IKMLoadingView *)showLoadingOnView:(UIView *)view{
    return [self showLoadingText:nil onView:view];
}

+ (IKMLoadingView *)showLoadingText:(NSString *)text{
    UIViewController *vc = [[UIApplication sharedApplication] currentViewController];
    return [self showLoadingText:text onView:vc.view enableEvent:NO];
}

+ (IKMLoadingView *)showLoading{
    if ([[[NSBundle mainBundle]bundleIdentifier] isEqualToString:@"com.ikang.official"]) {/////////爱康
        return [self showLoadingText:@"正在加载"];
    }else{//.//////体检宝
       return [self showLoadingText:nil];
    }
    
}

+ (IKMLoadingView *)showLoadingOnWindow{
    return [self showLoadingTextOnWindow:nil];
}

+ (IKMLoadingView *)showLoadingTextOnWindow:(NSString *)text{
    return [self showLoadingText:text onView:nil enableEvent:YES];
}

/** 移除loading图 */
+ (void)hidenLoading{
    UIViewController *vc = [[UIApplication sharedApplication] currentViewController];
    [self hidenLoadingOnView:vc.view];
}

+ (void)hidenLoadingOnWindow{
    [self hidenLoadingOnView:[[UIApplication sharedApplication] mainWindow]];
}

+ (void)hidenLoadingOnView:(UIView *)view{
    NSEnumerator *subviewsEnum = [view.subviews reverseObjectEnumerator];
    for (UIView *subview in subviewsEnum) {
        if ([subview isKindOfClass:[IKMLoadingView class]]) {
            IKMLoadingView *loadingView = (IKMLoadingView *)subview ;
            [self hidenLoading:loadingView];
        }
    }
}

+ (void)hidenLoading:(IKMLoadingView *)loadingView{
    [loadingView removeFromSuperview];
}

#pragma mark - popview

+(IKMPopView *)showPopViewCenter:(UIView *)contentView
                       animsted:(BOOL)animsted{
    IKMPopView *popView = [IKMPopView popContentView:contentView animated:animsted];
    return popView;
}

+(IKMPopView *)showPopView:(UIView *)contentView
                 showType:(PopViewShowType)showType{
    IKMPopView *popView = [IKMPopView popContentView:contentView withShowType:showType];
    return popView;
}

+ (void)hidenPopView{
    [IKMPopView hidenPopView];
}

#pragma mark - alert

+ (IKMAlertView *)showAlertWithTitle:(NSString *)title
                               image:(UIImage *)image
                         messageText:(id)message
                     leftButtonTitle:(NSString *)leftTitle
                    rightButtonTitle:(NSString *)rigthTitle
                           leftBlock:(dispatch_block_t)leftBlock
                          rightBlock:(dispatch_block_t)rightBlock{
    
    IKMAlertView *alertView = [[IKMAlertView alloc] initWithTitle:title
                                                              image:image
                                                        messageText:message
                                                    leftButtonTitle:leftTitle
                                                   rightButtonTitle:rigthTitle];
    alertView.leftBlock = leftBlock;
    alertView.rightBlock = rightBlock;
    
    alertView.dismissBlock = ^{
        [self dismissAlert];
    };
    
    [self dismissAlert];

    [[[UIApplication sharedApplication] mainWindow] addSubview:alertView];

    [alertView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    return alertView;
}

+(void)dismissAlert{
    NSEnumerator *subviewsEnum = [[[UIApplication sharedApplication] mainWindow].subviews reverseObjectEnumerator];
    for (UIView *subview in subviewsEnum) {
        if ([subview isKindOfClass:[IKMAlertView class]]) {
            IKMAlertView *alertView = (IKMAlertView *)subview ;
            [alertView removeFromSuperview];
        }
    }
}
@end
