//
//  IKMToastView.h
//  IKEasyShowView
//
//  Created by 狄烨 . on 2018/11/22.
//  Copyright © 2018 HU. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IKMToastView : UIView
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) UIImage *image;
@end

NS_ASSUME_NONNULL_END
