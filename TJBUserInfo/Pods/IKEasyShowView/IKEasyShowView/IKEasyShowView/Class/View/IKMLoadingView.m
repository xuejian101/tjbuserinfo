//
//  IKMLoadingView.m
//  IKEasyShowView
//
//  Created by 狄烨 . on 2018/11/22.
//  Copyright © 2018 HU. All rights reserved.
//

#import "IKMLoadingView.h"
#import <Masonry/Masonry.h>

@interface IKMLoadingView ()
@property (nonatomic,strong)JHButton *loadingButton;
@end

@implementation IKMLoadingView

- (instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = [EasyShowConfig shared].loadingBgColor;
        //------- loading imageView -------//
        UIView *bgView = [[UIView alloc] init];
        [self addSubview:bgView];
        bgView.backgroundColor = [EasyShowConfig shared].loadingTintColor;
        bgView.layer.cornerRadius = [EasyShowConfig shared].loadingCornerRadius;
        if ([EasyShowConfig shared].loadingShadowColor!=[UIColor clearColor]) {
            bgView.layer.shadowColor = [EasyShowConfig shared].loadingShadowColor.CGColor;
            bgView.layer.shadowOpacity = [EasyShowConfig shared].loadingShadowOpacity;
            bgView.layer.shadowRadius = [EasyShowConfig shared].loadingShadowRadius;
            bgView.layer.shadowOffset = CGSizeZero;
        }

        _loadingButton = [[JHButton alloc] initWithType:[EasyShowConfig shared].loadingType AndMarginArr:@[[NSNumber numberWithFloat:[EasyShowConfig shared].loadingSpace]]];
        _loadingButton.backgroundColor = [UIColor clearColor];
        
        if ([EasyShowConfig shared].loadingImagesArray.count>0) {
            UIImage *image = [EasyShowConfig shared].loadingImagesArray.firstObject;
            _loadingButton.image = image;
            _loadingButton.imageView.animationImages = [EasyShowConfig shared].loadingImagesArray;
            _loadingButton.imageView.animationDuration = [EasyShowConfig shared].loadingAnimationTime;
            _loadingButton.imageView.animationRepeatCount = 0;
            [_loadingButton.imageView startAnimating];
        }else{
            UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            activityIndicatorView.color = [EasyShowConfig shared].activityColor;
            CGAffineTransform transform = CGAffineTransformMakeScale(1.2f, 1.2f);
            activityIndicatorView.transform = transform;
            [activityIndicatorView startAnimating];
            [_loadingButton.imageView addSubview:activityIndicatorView];
            
            [activityIndicatorView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.left.equalTo(activityIndicatorView.superview).offset(10);;
                make.right.bottom.equalTo(activityIndicatorView.superview).offset(-10);
            }];
        }
        
        _loadingButton.contentLabel.textColor = [EasyShowConfig shared].loadingTextColor;
        _loadingButton.contentLabel.font = [EasyShowConfig shared].loadingTextFont;
        _loadingButton.contentLabel.numberOfLines = 0;
        _loadingButton.contentLabel.textAlignment =NSTextAlignmentCenter;
        [bgView addSubview:_loadingButton];
        
        // 设置背景view的约束
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(bgView.superview);
        }];
        
        // 设置label的约束
        [_loadingButton.contentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_lessThanOrEqualTo([EasyShowConfig shared].loadingMaxWidth);
            make.height.mas_lessThanOrEqualTo([EasyShowConfig shared].loadingMaxHeight);
        }];
        
        [_loadingButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.loadingButton.superview).offset([EasyShowConfig shared].loadingVerticalPadding);
            make.left.equalTo(self.loadingButton.superview).offset([EasyShowConfig shared].loadingHorizontalPadding);
            make.bottom.equalTo(self.loadingButton.superview).offset(-[EasyShowConfig shared].loadingVerticalPadding);
            make.right.equalTo(self.loadingButton.superview).offset(-[EasyShowConfig shared].loadingHorizontalPadding);
        }];
    }
    return self;
}

#pragma mark - 赋值
/** 赋值loading说明信息 */
- (void)setText:(NSString *)text{
    _text = text;
    _loadingButton.text = text;
}
@end
