//
//  IKMPopView.m
//  IKEasyShowView
//
//  Created by 狄烨 . on 2018/11/22.
//  Copyright © 2018 HU. All rights reserved.
//

#import "IKMPopView.h"
#import "EasyShowConfig.h"
#import <IKMediator/UIApplication+GetRootVC.h>
static const CGFloat animationTime = 0.25;
@interface IKMPopView()<UIGestureRecognizerDelegate>
@property (nonatomic ,weak) UIView *contentView;
@property (nonatomic ,assign) BOOL animation;
@property (nonatomic,assign) PopViewShowType showType;
@property (nonatomic,assign) CGRect showFrame;
@property (nonatomic,assign) CGRect hideFrame;
@property (nonatomic ,strong) UIView *popContainerView; //包含要显示的View的父控件
@property (nonatomic,assign) CGRect contentFrame;
@end

@implementation IKMPopView

- (instancetype)initWithFrame:(CGRect)frame
                  contentView:(UIView *)contentView
                     showType:(PopViewShowType)showType
                    animation:(BOOL)animation{
    self = [super initWithFrame:frame];
    if (self) {
        self.animation = animation;
        self.contentView = contentView;
        self.contentFrame = contentView.frame;
        self.clickOutHidden = YES;
        if (showType!=0) {
            self.showType = showType;
        }
        UIControl *backCtl = [[UIControl alloc] initWithFrame:self.bounds];
        [self addSubview:backCtl];
        [backCtl addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
        
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    }
    return self;
}

- (void)setShowType:(PopViewShowType)showType{
    _showType = showType;
    /**
     *  计算出_contentView的动画的 起始位置 和 中止位置
     */
    if (_showType == PopViewShowTypeTop) {
        _showFrame = CGRectMake(CGRectGetMinX(_contentFrame), CGRectGetMinY(_contentFrame), CGRectGetWidth(_contentFrame), CGRectGetHeight(_contentFrame));
        _hideFrame = CGRectMake(CGRectGetMinX(_contentFrame), -CGRectGetMaxY(_contentFrame), CGRectGetWidth(_contentFrame), CGRectGetHeight(_contentFrame));
    }
    if (_showType == PopViewShowTypeBottom) {
        _showFrame = CGRectMake(CGRectGetMinX(_contentFrame), CGRectGetHeight(self.frame)-HOME_HEIGHT-CGRectGetMaxY(_contentFrame), CGRectGetWidth(_contentFrame), CGRectGetHeight(_contentFrame));
        _hideFrame = CGRectMake(CGRectGetMinX(_contentFrame), CGRectGetMaxY(self.frame)+CGRectGetMinY(_contentFrame),CGRectGetWidth(_contentFrame), CGRectGetHeight(_contentFrame));
    }
    if (_showType == PopViewShowTypeLeft) {
        _showFrame = CGRectMake(CGRectGetMinX(_contentFrame), CGRectGetMinY(_contentFrame), CGRectGetWidth(_contentFrame), CGRectGetHeight(_contentFrame));
        _hideFrame = CGRectMake(-CGRectGetWidth(_contentFrame), CGRectGetMinY(_contentFrame), CGRectGetWidth(_contentFrame), CGRectGetHeight(_contentFrame));
    }
    if (_showType == PopViewShowTypeRight) {
        _showFrame = CGRectMake(CGRectGetWidth(self.frame)-CGRectGetWidth(_contentFrame), CGRectGetMinY(_contentFrame), CGRectGetWidth(_contentFrame), CGRectGetHeight(_contentFrame));
        _hideFrame = CGRectMake(CGRectGetWidth(self.frame), CGRectGetMinY(_contentFrame), CGRectGetWidth(_contentFrame), CGRectGetHeight(_contentFrame));
    }
    self.popContainerView.frame = _hideFrame;
}

- (void)backClick{
    if (self.clickOutHidden) {
        [IKMPopView hidenPopView];
    }
}

- (void)remove{
    if (self.hiddenPop) {
        self.hiddenPop();
    }
    [super removeFromSuperview];
}

- (UIView *)popContainerView{
    if (!_popContainerView) {
        _popContainerView = [[UIView alloc] init];
    }
    return _popContainerView;
}

#pragma mark - 类方法

+ (instancetype)getCurrentPopView{
    NSEnumerator *subviewsEnum = [[[UIApplication sharedApplication] mainWindow].subviews reverseObjectEnumerator];
    for (UIView *subview in subviewsEnum) {
        if ([subview isKindOfClass:[IKMPopView class]]) {
            IKMPopView *showView = (IKMPopView *)subview;
            return showView;
        }
    }
    return nil;
}

+ (instancetype)popContentView:(UIView *)contentView animated:(BOOL)animated{
    IKMPopView *oldPopView = [self getCurrentPopView];
    [oldPopView removeFromSuperview];
    
    IKMPopView *newPopView = [[IKMPopView alloc] initWithFrame:[[UIApplication sharedApplication] mainWindow].bounds
                                                 contentView:contentView
                                                    showType:0
                                                   animation:animated];
    [[[UIApplication sharedApplication] mainWindow] addSubview:newPopView];
    [newPopView addSubview:contentView];
    contentView.center = newPopView.center;
    
    newPopView.clipsToBounds = YES;
    if (animated) {
        contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity,
                                                       CGFLOAT_MIN, CGFLOAT_MIN);
        [UIView animateWithDuration:animationTime animations:^{
            contentView.transform = CGAffineTransformIdentity;
        }];
    }
    return newPopView;
}

+ (instancetype)popContentView:(UIView *)contentView withShowType:(PopViewShowType)type{
    IKMPopView *oldPopView = [self getCurrentPopView];
    [oldPopView removeFromSuperview];
    
    IKMPopView *newPopView = [[IKMPopView alloc] initWithFrame:[[UIApplication sharedApplication] mainWindow].bounds
                                                 contentView:contentView
                                                    showType:type
                                                   animation:YES];
    [[[UIApplication sharedApplication] mainWindow] addSubview:newPopView];
    [newPopView.popContainerView addSubview:contentView];
    [newPopView addSubview:newPopView.popContainerView];
    
    [UIView animateWithDuration:animationTime animations:^{
        newPopView.popContainerView.frame = newPopView.showFrame;
    }];
    return newPopView;
}

+ (void)hidenPopView{
    IKMPopView *popView = [self getCurrentPopView];
    if (popView.showType != 0) {
        [UIView animateWithDuration:animationTime
                         animations:^{
                             popView.popContainerView.frame = popView.hideFrame;
                         }
                         completion:^(BOOL finished) {
                             [popView remove];
                         }];
    }else{
        if (popView.animation) {
            [UIView animateWithDuration:animationTime
                             animations:^{
                                 popView.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
                             }
                             completion:^(BOOL finished) {
                                 [popView remove];
                             }];
        } else {
            [popView remove];
        }
    }
    
}
@end
