//
//  IKMToastView.m
//  IKEasyShowView
//
//  Created by 狄烨 . on 2018/11/22.
//  Copyright © 2018 HU. All rights reserved.
//

#import "IKMToastView.h"
#import <Masonry/Masonry.h>
#import "EasyShowConfig.h"

@interface IKMToastView ()
@property (nonatomic,strong)JHButton *toastButton;
@end

@implementation IKMToastView

- (instancetype)init{
    if (self = [super init]) {
        
        UIView *bgView = [[UIView alloc] init];
        [self addSubview:bgView];
        bgView.backgroundColor = [EasyShowConfig shared].toastBgColor;
        bgView.layer.cornerRadius = [EasyShowConfig shared].toastCornerRadius;
        if ([EasyShowConfig shared].toastShadowColor!=[UIColor clearColor]) {
            bgView.layer.shadowColor = [EasyShowConfig shared].toastShadowColor.CGColor;
            bgView.layer.shadowOpacity = [EasyShowConfig shared].toastShadowOpacity;
            bgView.layer.shadowRadius = [EasyShowConfig shared].toastShadowRadius;
            bgView.layer.shadowOffset = CGSizeZero;
        }
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.bottom.equalTo(bgView.superview);
        }];
        
        _toastButton = [[JHButton alloc] initWithType:[EasyShowConfig shared].toastType AndMarginArr:@[[NSNumber numberWithFloat:[EasyShowConfig shared].toastSpace]]];
        _toastButton.backgroundColor = [UIColor clearColor];
        _toastButton.contentLabel.textColor = [EasyShowConfig shared].toastTextColor;
        _toastButton.contentLabel.font = [EasyShowConfig shared].toastTextFont;
        _toastButton.contentLabel.numberOfLines = 0;
        _toastButton.contentLabel.textAlignment =NSTextAlignmentCenter;
        [bgView addSubview:_toastButton];
        
        // 设置label的约束
        [_toastButton.contentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_lessThanOrEqualTo([EasyShowConfig shared].toastMaxWidth);
            make.height.mas_lessThanOrEqualTo([EasyShowConfig shared].toastMaxHeight);
        }];
        
        [_toastButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.equalTo(self.toastButton.superview).offset([EasyShowConfig shared].toastPadding);
            make.bottom.right.equalTo(self.toastButton.superview).offset(-[EasyShowConfig shared].toastPadding);
        }];
    }
    return self;
}
-(void)setText:(NSString *)text{
    _text= text;
    _toastButton.text = text;
}
-(void)setImage:(UIImage *)image{
    _image= image;
    _toastButton.image = image;
}
@end
