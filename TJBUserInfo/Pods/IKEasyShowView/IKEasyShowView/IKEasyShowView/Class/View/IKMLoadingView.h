//
//  IKMLoadingView.h
//  IKEasyShowView
//
//  Created by 狄烨 . on 2018/11/22.
//  Copyright © 2018 HU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EasyShowConfig.h"
NS_ASSUME_NONNULL_BEGIN

@interface IKMLoadingView : UIView
/** loading信息 */
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSArray *imageArray;

@end

NS_ASSUME_NONNULL_END
