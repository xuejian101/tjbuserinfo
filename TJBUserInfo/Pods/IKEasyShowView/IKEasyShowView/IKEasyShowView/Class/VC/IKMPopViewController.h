//
//  IKMPopViewController.h
//  IKToolsModule
//
//  Created by 狄烨 . on 2018/12/3.
//  Copyright © 2018 HU. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IKMPopViewController : UIViewController
@property(nonatomic, weak) UIView *popView;
- (void)updateContentSize;
@end

NS_ASSUME_NONNULL_END
