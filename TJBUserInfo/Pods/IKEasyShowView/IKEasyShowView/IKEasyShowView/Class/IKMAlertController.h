//
//  IKMAlertController.h
//  IKEasyShowView
//
//  Created by 狄烨 . on 2018/12/26.
//  Copyright © 2018 HU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EasyShowConfig.h"
NS_ASSUME_NONNULL_BEGIN
/**
 参考JXTAlertController封装
 添加TextField
 [alertMaker addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
 textField.placeholder = @"输入框1-请输入";
 }];
 获取
 UITextField *textField = alertSelf.textFields.firstObject;
 textField.text
 */

#define DefaultTitleTextColor              ColorFromName(0x333333)
#define DefaultMessageTextColor            ColorFromName(0x333333)
#define DefaultTitleTextFont               [UIFont systemFontOfSize:17]
#define DefaultMessageTextFont             [UIFont systemFontOfSize:15]
#define DefaultCancelTextColor             ColorFromName(0x333333)
#define DefaultActionTextColor             ColorFromName(0xea5504)

@class IKMAlertController;
/**
 IKMAlertController: alertAction配置链
 */
typedef IKMAlertController * _Nonnull (^IKMAlertAction)(NSString *title);
typedef IKMAlertController * _Nonnull (^IKMAlertCustomAction)(NSString *title,UIColor *titleColor);
/**
 IKMAlertController: alert构造块
 @param alertMaker IKMAlertController配置对象
 */
typedef void(^IKMAlertAppearanceProcess)(IKMAlertController *alertMaker);

/**
 IKMAlertController: alert按钮执行回调
 @param buttonIndex 按钮index(根据添加action的顺序)
 @param action      UIAlertAction对象
 @param alertSelf   本类对象
 */
typedef void (^IKMAlertActionBlock)(NSInteger buttonIndex, UIAlertAction *action, IKMAlertController *alertSelf);

@interface IKMAlertController : UIAlertController
/**
 IKMAlertController: 禁用alert弹出动画，默认执行系统的默认弹出动画
 */
- (void)alertAnimateDisabled;
/**
 IKMAlertController: alert弹出后，可配置的回调
 */
@property (nullable, nonatomic, copy) void (^alertDidShow)(void);

/**
 IKMAlertController: alert关闭后，可配置的回调
 */
@property (nullable, nonatomic, copy) void (^alertDidDismiss)(void);

/**
 IKMAlertController: 设置toast模式展示时间：如果alert未添加任何按钮，将会以toast样式展示，这里设置展示时间，默认1s
 */
@property (nonatomic, assign) NSTimeInterval toastStyleDuration; //deafult alertShowDurationDefault = 1s

- (IKMAlertAction)addDefaultAction;

- (IKMAlertCustomAction)addAttributedAction;

- (IKMAlertAction)addCancelAction;

- (IKMAlertAction)addDestructiveAction;

/**
 IKMAlertController: show-alert
 @param title             title
 @param message           message
 @param appearanceProcess alert配置过程
 @param actionBlock       alert点击响应回调
 */
+ (void)showAlertWithTitle:(nullable NSString *)title
                   message:(nullable NSString *)message
         appearanceProcess:(IKMAlertAppearanceProcess)appearanceProcess
              actionsBlock:(nullable IKMAlertActionBlock)actionBlock;
/**
 IKMAlertController: show-actionSheet
 @param title             title
 @param message           message
 @param appearanceProcess actionSheet配置过程
 @param actionBlock       actionSheet点击响应回调
 */
+ (void)showActionSheetWithTitle:(nullable NSString *)title
                         message:(nullable NSString *)message
               appearanceProcess:(IKMAlertAppearanceProcess)appearanceProcess
                    actionsBlock:(nullable IKMAlertActionBlock)actionBlock;
@end

@interface UIAlertController (Color)

@property (nonatomic,strong) UIColor *titleColor; /**< 标题的颜色 */
@property (nonatomic,strong) UIColor *messageColor; /**< 信息的颜色 */

@property (nonatomic,strong) UIFont *titleFont; /**< 标题的字体 */
@property (nonatomic,strong) UIFont *messageFont; /**< 信息的字体 */
@end
NS_ASSUME_NONNULL_END
