//
//  UIFont+Loader.h
//  IKResourcesModule
//
//  Created by HU on 2018/6/7.
//  Copyright © 2018年 HU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Loader)
+ (UIFont *)loadFontOfSize:(CGFloat)size;
@end
