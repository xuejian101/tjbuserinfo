//
//  UIFont+Loader.m
//  IKResourcesModule
//
//  Created by HU on 2018/6/7.
//  Copyright © 2018年 HU. All rights reserved.
//

#import "UIFont+Loader.h"
#import <CoreText/CTFontManager.h>

@implementation UIFont (Loader)
+ (UIFont *)loadFontOfSize:(CGFloat)size
{
    NSString *fontName = @"unsplashFont";
    UIFont *font = [UIFont fontWithName:fontName size:size];
    if (!font) {
        [[self class] dynamicallyLoadFontNamed:fontName];
        font = [UIFont fontWithName:fontName size:size];
        if (!font) font = [UIFont systemFontOfSize:size];
    }
    return font;
}

+ (void)dynamicallyLoadFontNamed:(NSString *)name
{
    NSString *fontfileName = @"iconfont.ttf";
    
    NSString *mainBundlePath = [NSBundle mainBundle].bundlePath;
    NSString *bundlePath = [NSString stringWithFormat:@"%@/%@",mainBundlePath,@"IKResourcesModule.bundle"];
    NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
    if (bundle == nil) {
        bundlePath = [NSString stringWithFormat:@"%@/%@",mainBundlePath,@"Frameworks/IKResourcesModule.framework/IKResourcesModule.bundle"];
        bundle = [NSBundle bundleWithPath:bundlePath];
    }
    NSString *resourcePath = [NSString stringWithFormat:@"%@/%@",bundle.bundlePath,fontfileName];
    
    NSURL *url = [NSURL fileURLWithPath:resourcePath];
    
    NSData *fontData = [NSData dataWithContentsOfURL:url];
    if (fontData) {
        CFErrorRef error;
        CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)fontData);
        CGFontRef font = CGFontCreateWithDataProvider(provider);
        if (! CTFontManagerRegisterGraphicsFont(font, &error)) {
            CFStringRef errorDescription = CFErrorCopyDescription(error);
            NSLog(@"Failed to load font: %@", errorDescription);
            CFRelease(errorDescription);
        }
        CFRelease(font);
        CFRelease(provider);
    }
}
@end
