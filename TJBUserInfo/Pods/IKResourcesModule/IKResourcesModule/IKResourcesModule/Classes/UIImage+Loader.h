//
//  UIImage+Loader.h
//  IKResourcesModule
//
//  Created by HU on 2018/6/7.
//  Copyright © 2018年 HU. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@interface UIImage (Loader)
+ (UIImage *)loadImageNamed:(NSString *)name;
@end
NS_ASSUME_NONNULL_END
