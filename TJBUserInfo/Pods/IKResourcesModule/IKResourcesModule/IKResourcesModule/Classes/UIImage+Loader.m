//
//  UIImage+Loader.m
//  IKResourcesModule
//
//  Created by HU on 2018/6/7.
//  Copyright © 2018年 HU. All rights reserved.
//

#import "UIImage+Loader.h"
NS_ASSUME_NONNULL_BEGIN
@implementation UIImage (Loader)
+ (UIImage *)loadImageNamed:(NSString *)name{
    NSString *mainBundlePath = [NSBundle mainBundle].bundlePath;
    NSString *bundlePath = [NSString stringWithFormat:@"%@/%@",mainBundlePath,@"IKResourcesModule.bundle"];
    NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
    if (bundle == nil) {
        bundlePath = [NSString stringWithFormat:@"%@/%@",mainBundlePath,@"Frameworks/IKResourcesModule.framework/IKResourcesModule.bundle"];
        bundle = [NSBundle bundleWithPath:bundlePath];
    }
    if ([UIImage respondsToSelector:@selector(imageNamed:inBundle:compatibleWithTraitCollection:)]) {
        return [UIImage imageNamed:name inBundle:bundle compatibleWithTraitCollection:nil];
    } else {
        return [UIImage imageWithContentsOfFile:[bundle pathForResource:name ofType:nil]];
    }
}
@end
NS_ASSUME_NONNULL_END
