#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "AppDelegateMediator.h"
#import "BaseMediator.h"
#import "IKMediator.h"
#import "UIApplication+GetRootVC.h"

FOUNDATION_EXPORT double IKMediatorVersionNumber;
FOUNDATION_EXPORT const unsigned char IKMediatorVersionString[];

