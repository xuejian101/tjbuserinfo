#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "IKCoreModule.h"
#import "IKMBaseModel.h"
#import "IKMBaseRequest.h"
#import "IKMNetworking.h"

FOUNDATION_EXPORT double IKCoreModuleVersionNumber;
FOUNDATION_EXPORT const unsigned char IKCoreModuleVersionString[];

