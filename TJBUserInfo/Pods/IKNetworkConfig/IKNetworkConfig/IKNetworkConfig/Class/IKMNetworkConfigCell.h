//
//  IKMNetworkConfigCell.h
//  IKNetworkConfig
//
//  Created by HU on 2018/7/30.
//  Copyright © 2018年 HU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKMNetworkConfigCell : UITableViewCell
@property (strong,nonatomic) NSDictionary *model;

@property (nonatomic,strong) UILabel * configLabel;
@end
