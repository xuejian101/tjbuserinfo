//
//  IKMNetworkConfigViewController.m
//  IKNetworkConfig
//
//  Created by HU on 2018/7/30.
//  Copyright © 2018年 HU. All rights reserved.
//

#import "IKMNetworkConfigViewController.h"
 
#import "IKMNetworkConfigCell.h"
@interface IKMNetworkConfigViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *configTableView;
@property (nonatomic, strong) NSString *selectedName;

@property (nonatomic, strong) NSIndexPath *previousIndex;
@end

@implementation IKMNetworkConfigViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"网络环境配置";
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *saveButton = [[UIButton alloc] init];
    saveButton.frame = CGRectMake(0, 0, 44, 40);
    [saveButton setTitle:@"确定" forState:UIControlStateNormal];
    [saveButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(buttonConfirm) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
    self.navigationItem.rightBarButtonItem = saveButtonItem;
    
    // 列表视图
    _configTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    _configTableView.delegate = self;
    _configTableView.dataSource = self;
    _configTableView.tableFooterView = [UIView new];
    _configTableView.showsHorizontalScrollIndicator = NO;
    _configTableView.showsVerticalScrollIndicator = NO;
    _configTableView.rowHeight = 120;
    _configTableView.delaysContentTouches = YES;
    [self.view addSubview:_configTableView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setConfigURLs:(NSDictionary *)configURLs{
    _configURLs = configURLs;
    [_configTableView reloadData];
}
-(void)setConfigName:(NSString *)configName{
    _configName = configName;
}
#pragma mark - tableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _configURLs.allKeys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IKMNetworkConfigCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    if (cell == nil) {
        cell = [[IKMNetworkConfigCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UITableViewCell"];
    }
    NSString *name = _configURLs.allKeys[indexPath.row];
    NSDictionary *dic = [_configURLs objectForKey:name];
    // 字体颜色
    cell.configLabel.text = name;
    cell.configLabel.textColor = [UIColor blackColor];
    cell.model = dic;
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if ([_configName isEqualToString:name]) {
        cell.configLabel.textColor = [UIColor redColor];
        _previousIndex = indexPath;
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // 当前选择回调 
    _selectedName = _configURLs.allKeys[indexPath.row];
    // 选择操作
    if (_previousIndex) {
        IKMNetworkConfigCell *cellPrevious = [tableView cellForRowAtIndexPath:_previousIndex];
        cellPrevious.accessoryType = UITableViewCellAccessoryNone;
        // 字体颜色
        cellPrevious.configLabel.textColor = [UIColor blackColor];
    }
    
    IKMNetworkConfigCell *cellSelected = [tableView cellForRowAtIndexPath:indexPath];
    cellSelected.accessoryType = UITableViewCellAccessoryCheckmark;
    // 字体高亮颜色
    cellSelected.configLabel.textColor = [UIColor redColor];
    
    _previousIndex = indexPath;
}

#pragma mark - 响应

- (void)buttonConfirm{
    // 保存
    if (_configSelected && _selectedName){
        _configSelected(_selectedName);
    }
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


@end
