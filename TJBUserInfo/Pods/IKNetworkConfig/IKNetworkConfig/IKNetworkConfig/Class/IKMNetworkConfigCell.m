//
//  IKMNetworkConfigCell.m
//  IKNetworkConfig
//
//  Created by HU on 2018/7/30.
//  Copyright © 2018年 HU. All rights reserved.
//

#import "IKMNetworkConfigCell.h"
@interface IKMNetworkConfigCell()

@property (nonatomic,strong) UILabel * oneLabel;
@property (nonatomic,strong) UILabel * twoLabel;
@property (nonatomic,strong) UILabel * threeLabel;
@property (nonatomic,strong) UILabel * fourLabel;
@end
@implementation IKMNetworkConfigCell
- (nonnull instancetype)initWithStyle:(UITableViewCellStyle)style
                      reuseIdentifier:(nullable NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        CGFloat offsetY = 5;
        
        _configLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, offsetY, self.frame.size.width, 20)];
        _configLabel.font = [UIFont systemFontOfSize:15];
        _configLabel.textColor = [UIColor blackColor];
        [self addSubview:_configLabel];
        
        offsetY+=20;
        offsetY+=5;
        
        _oneLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, offsetY, self.frame.size.width, 15)];
        _oneLabel.font = [UIFont systemFontOfSize:10];
        _oneLabel.textColor = [UIColor blackColor];
        [self addSubview:_oneLabel];
        
        offsetY+=15;
        offsetY+=5;
        
        _twoLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, offsetY, self.frame.size.width, 15)];
        _twoLabel.font = [UIFont systemFontOfSize:10];
        _twoLabel.textColor = [UIColor blackColor];
        [self addSubview:_twoLabel];
        
        offsetY+=15;
        offsetY+=5;
        
        _threeLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, offsetY, self.frame.size.width, 15)];
        _threeLabel.font = [UIFont systemFontOfSize:10];
        _threeLabel.textColor = [UIColor blackColor];
        [self addSubview:_threeLabel];
        
        offsetY+=15;
        offsetY+=5;
        
        _fourLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, offsetY, self.frame.size.width, 15)];
        _fourLabel.font = [UIFont systemFontOfSize:10];
        _fourLabel.textColor = [UIColor blackColor];
        [self addSubview:_fourLabel];
    }
    
    return self;
}

- (void)setModel:(NSDictionary *)model{
 
    _oneLabel.text = [NSString stringWithFormat:@"APP服务--->%@",model[@"APPHostURL"]];
    _twoLabel.text = [NSString stringWithFormat:@"统一用户服务--->%@",model[@"UnitUserHostURL"]];
    _threeLabel.text = [NSString stringWithFormat:@"H5服务--->%@",model[@"H5SetHostURL"]];
    _fourLabel.text = [NSString stringWithFormat:@"H5报告独立化--->%@",model[@"H5ReportURL"]];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
