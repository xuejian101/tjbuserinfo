//
//  UIApplication+TJBAppNet.m
//  IKAppNetworkConfig
//
//  Created by HU on 2018/7/24.
//  Copyright © 2018年 HU. All rights reserved.
//

#import "UIApplication+TJBAppNet.h"
#import "IKMNetworkConfig.h"
@implementation UIApplication (TJBAppNet)
+ (void)load{
    [super load];
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ //只执行一次就可以了
        NetworkConfig.configHost = @"0";
        NetworkConfig.configHostDebug = @{@"APPHostURL"         :@"http://uat.tjbappweb.ikang.com",
                                          @"UnitUserHostURL"    :@"https://newuat-oauth2.health.ikang.com",
                                          @"H5ReportURL"        :@"https://uat-report.health.ikang.com",////报告独立化
                                          @"H5SetHostURL"       :@"http://newuat.im.ikang.com", //H5 host地址
                                          @"PayHostURL"         :@"https://newuat-pay.health.ikang.com",
                                          @"UPPayModel"         :@"00",
                                          @"HostModel"          :@(3),///////UAT
                                          @"APPPathURL"         :@"api"  ///////体检宝APP标记
                                          };
        NetworkConfig.configHostRelease = @{@"APPHostURL"       :@"https://tjbappweb.health.ikang.com",
                                            @"UnitUserHostURL"  :@"https://oauth2.health.ikang.com",
                                            @"H5ReportURL"      :@"https://report.health.ikang.com",
                                            @"H5SetHostURL"     :@"http://m.ikang.com", ///H5 host地址
                                            @"PayHostURL"       :@"https://pay.health.ikang.com",
                                            @"UPPayModel"       :@"00",
                                            @"HostModel"        :@(1),//////Release
                                            @"APPPathURL"       :@"api"  ///////体检宝APP标记
                                            };
        NetworkConfig.configHostDebugDict = @{@"预生产":@{@"APPHostURL"        :@"https://pre-tjbappweb.health.ikang.com",
                                                         @"UnitUserHostURL"   :@"https://oauth2.health.ikang.com",
                                                         @"H5ReportURL"       :@"https://pre-report.health.ikang.com",
                                                         @"H5SetHostURL"      :@"http://pre.im.ikang.com", ///H5 host地址
                                                         @"PayHostURL"        :@"https://pay.health.ikang.com",
                                                         @"UPPayModel"        :@"00",
                                                         @"HostModel"         :@(2),////Pre
                                                         @"APPPathURL"        :@"api"  ///////体检宝APP标记
                                                       },
                                              @"测试":@{  @"APPHostURL"        :@"http://test.ikapp.ikang.com",
                                                         @"UnitUserHostURL"   :@"http://192.168.99.194:8964",
                                                         @"H5ReportURL"       :@"https://test-report.health.ikang.com",
                                                         @"H5SetHostURL"      :@"http://test.im.ikang.com", ///H5 host地址
                                                         @"PayHostURL"        :@"http://192.168.99.152:8080",
                                                         @"UPPayModel"        :@"01",
                                                         @"HostModel"         :@(4),/////Test
                                                         @"APPPathURL"        :@"api"  ///////体检宝APP标记
                                                       }};
        [NetworkConfig initializeConfig];
    });
}

@end
