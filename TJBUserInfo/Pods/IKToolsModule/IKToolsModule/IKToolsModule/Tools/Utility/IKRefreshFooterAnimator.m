//
//  IKRefreshFooterAnimator.m
//  IKToolsModule
//
//  Created by 狄烨 . on 2019/6/4.
//  Copyright © 2019 HU. All rights reserved.
//

#import "IKRefreshFooterAnimator.h"
#import "UILineView.h"
#import "IKToolsDefine.h"
@interface IKRefreshFooterAnimator()
/** 所有状态对应的文字 */
@property (weak, nonatomic) UILabel *stateLabel;
@property (weak, nonatomic) UIActivityIndicatorView *loading;
@property (weak, nonatomic) UILineView *leftlineView;
@property (weak, nonatomic) UILineView *rightlineView;
@end
@implementation IKRefreshFooterAnimator
#pragma mark - 私有方法
- (void)stateLabelClick{
    if (self.state == MJRefreshStateIdle) {
        [self beginRefreshing];
    }
}
#pragma mark - 重写父类的方法
- (void)prepare{
    [super prepare];
    self.mj_h = 73;
    // 初始化文字
    // 添加label
    UILabel *label = [[UILabel alloc] init];
    label.textColor = UIColorFromName(0xa3a3a3);
    label.font = [UIFont boldSystemFontOfSize:12];
    label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:label];
    self.stateLabel = label;
    
    // 监听label
    self.stateLabel.userInteractionEnabled = YES;
    [self.stateLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stateLabelClick)]];
    
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self addSubview:loading];
    self.loading = loading;
    
    UILineView *leftlineView = [[UILineView alloc] init];
    [self addSubview:leftlineView];
    self.leftlineView=leftlineView;
    
    UILineView *rightlineView = [[UILineView alloc] init];
    [self addSubview:rightlineView];
    self.rightlineView=rightlineView;
}
- (void)placeSubviews{
    [super placeSubviews];
    
    [self.stateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.equalTo(self);
    }];
    
    [self.loading mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self.stateLabel.mas_left).offset(-10);
    }];

    [self.leftlineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.stateLabel.mas_left).offset(-10);
        make.centerY.equalTo(self);
        make.width.mas_equalTo(20);
        make.height.mas_equalTo(LineHeight);
    }];
    
    [self.rightlineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.stateLabel.mas_right).offset(10);
        make.centerY.equalTo(self);
        make.width.mas_equalTo(20);
        make.height.mas_equalTo(LineHeight);
    }];
    
}
- (void)setState:(MJRefreshState)state{
    MJRefreshCheckState
    switch (state) {
        case MJRefreshStateIdle:
            self.stateLabel.text = @"加载更多数据";
            [self.loading stopAnimating];
            self.leftlineView.hidden = YES;
            self.rightlineView.hidden = YES;
            break;
        case MJRefreshStateRefreshing:
            self.stateLabel.text = @"加载中...";
            [self.loading startAnimating];
            self.leftlineView.hidden = YES;
            self.rightlineView.hidden = YES;
            break;
        case MJRefreshStateNoMoreData:
            self.stateLabel.text = @"没有更多了";
            [self.loading stopAnimating];
            self.leftlineView.hidden = NO;
            self.rightlineView.hidden = NO;
            break;
        default:
            break;
    }
}
#pragma mark 监听scrollView的contentOffset改变
- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change{
    [super scrollViewContentOffsetDidChange:change];
    
}

#pragma mark 监听scrollView的contentSize改变
- (void)scrollViewContentSizeDidChange:(NSDictionary *)change{
    [super scrollViewContentSizeDidChange:change];
    
}

#pragma mark 监听scrollView的拖拽状态改变
- (void)scrollViewPanStateDidChange:(NSDictionary *)change{
    [super scrollViewPanStateDidChange:change];
    
}
@end
