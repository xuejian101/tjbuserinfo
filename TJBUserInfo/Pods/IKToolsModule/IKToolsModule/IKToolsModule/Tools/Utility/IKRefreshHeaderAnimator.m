//
//  IKRefreshHeaderAnimator.m
//  IKToolsModule
//
//  Created by HU on 2018/7/31.
//  Copyright © 2018年 HU. All rights reserved.
//

#import "IKRefreshHeaderAnimator.h" 
#import "UIImage+BaseUI.h"
@interface IKRefreshHeaderAnimator ()
@property (nonatomic, strong) NSMutableArray *gifViewImages;
@end

@implementation IKRefreshHeaderAnimator

- (void)prepare{
    [super prepare];
    self.mj_h = 60;
    self.gifViewImages = [NSMutableArray array];
    
    if ([[[NSBundle mainBundle]bundleIdentifier] isEqualToString:@"com.ikang.official"]) {/////////爱康
        for (NSUInteger i = 0; i <= 11; i++) {
            UIImage *image = [UIImage loadToolImageNamed:[NSString stringWithFormat:@"loading_image_%lu",(unsigned long)i]];
            [self.gifViewImages addObject:image];
        }
    }else{//.//////体检宝
        for (NSUInteger i = 0; i <= 19; i++) {
            UIImage *image = [UIImage loadToolImageNamed:[NSString stringWithFormat:@"loading_%lu",(unsigned long)i]];
            [self.gifViewImages addObject:image];
        }
    }
    // 设置普通状态的动画图片
    [self setImages:self.gifViewImages forState:MJRefreshStateIdle];
    // 设置即将刷新状态的动画图片（一松开就会刷新的状态）
    [self setImages:self.gifViewImages forState:MJRefreshStatePulling];
    // 设置正在刷新状态的动画图片
    [self setImages:self.gifViewImages forState:MJRefreshStateRefreshing];
}

- (void)placeSubviews{
    [super placeSubviews];
    self.backgroundColor = [UIColor whiteColor];
    self.stateLabel.hidden = YES;
    self.lastUpdatedTimeLabel.hidden = YES;
    self.gifView.frame = self.bounds;
    self.gifView.contentMode = UIViewContentModeCenter;
}

@end
