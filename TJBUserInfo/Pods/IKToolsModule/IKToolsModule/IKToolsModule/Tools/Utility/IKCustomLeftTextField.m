//
//  IKCustomLeftTextField.m
//  IKToolsModule
//
//  Created by iOS123 on 2019/1/30.
//  Copyright © 2019 HU. All rights reserved.
//

#import "IKCustomLeftTextField.h"
#import "IKToolsDefine.h"
#import "Category.h"
#import "Utility.h"

static float leftGap = 20.0;
@interface IKCustomLeftTextField()
@property (nonatomic,strong) EdgeInsetsLabel *titleLabel;
@property (nonatomic,strong) UIView *lineView;
@property (nonatomic,strong) UIButton * idTypeButton;
@end

@implementation IKCustomLeftTextField

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        self.textColor = [UIColor baseTextColor];
        self.font = Font(15);
        self.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self buildUI];
    }
    return self;
}

- (void)buildUI{
    self.leftView = self.titleLabel;
    self.leftViewMode = UITextFieldViewModeAlways;
    
    [self addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftGap);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(LineHeight);
    }];
}

- (UIView*)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = [UIColor baseLineColor];
    }
    return _lineView;
}

- (EdgeInsetsLabel*)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[EdgeInsetsLabel alloc]initWithFrame:CGRectMake(0,0, 105, 50)];
        _titleLabel.text = @"";
        _titleLabel.font = Font(14);
        _titleLabel.numberOfLines = 1;
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textColor = [UIColor baseTextColor];
        _titleLabel.contentInset = UIEdgeInsetsMake(0, leftGap, 0, 0);
    }
    return _titleLabel;
}


- (UIButton*)idTypeButton{
    if (!_idTypeButton) {
        WS(ws)
        _idTypeButton = [[UIButton alloc]initWithFrame:CGRectMake(0,0, 50, 50)];
        _idTypeButton.backgroundColor = [UIColor whiteColor];
        [_idTypeButton setImage:[UIImage loadToolImageNamed:@"ic_arrow_gray_down"] forState:UIControlStateNormal];
        [_idTypeButton addActionforControlEvents:UIControlEventTouchUpInside Completion:^{
            if (ws.block) {
                ws.block();
            }
        }];
    }
    return _idTypeButton;
}


- (void)setTitle:(NSString *)title{
    _title = title;
    self.titleLabel.text = _title;
}

- (void)setRightBtn:(BOOL)rightBtn{
    self.rightView = self.idTypeButton;
    self.rightViewMode = UITextFieldViewModeAlways;
}
@end
