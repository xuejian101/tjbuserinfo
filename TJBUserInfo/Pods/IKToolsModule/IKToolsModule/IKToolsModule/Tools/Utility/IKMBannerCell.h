//
//  IKMBannerCell.h
//  IKToolsModule
//
//  Created by HU on 2018/7/30.
//  Copyright © 2018年 HU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/SDWebImage.h>
@interface IKMBannerCell : UICollectionViewCell
@property (nonatomic, strong) SDAnimatedImageView *imageView;
@end
