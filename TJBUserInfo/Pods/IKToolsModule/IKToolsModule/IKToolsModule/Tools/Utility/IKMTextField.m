//
//  IKMTextField.m
//  IKAppointmentModule
//
//  Created by SJZ on 2019/1/2.
//  Copyright © 2019 iOS123. All rights reserved.
//

#import "IKMTextField.h"
#import "Category.h"
#import "IKToolsDefine.h"

@interface IKMTextField () <UITextViewDelegate>
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UITextView * textView;
@property (nonatomic, strong) UILabel * textLabel;
@property (nonatomic, strong) UIButton * lookButton;
@property (nonatomic, strong) CAShapeLayer * lineLayer;
@property (nonatomic, strong) NSString * cipherTextStr;

@end

@implementation IKMTextField

- (instancetype)initWithType:(IKMTextFieldType)fieldType {
    self = [super init];
    if(self) {
        self.backgroundColor = [UIColor whiteColor];
        _fieldType = fieldType;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterForegroundNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
        _placeholderColor = [UIColor colorWithHex:0x999999];
        _textColor = [UIColor baseTextColor];
        [self buildUI];
    }
    
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)enterForegroundNotification:(NSNotification *)noti {
    [_lineLayer addAnimation:[self opacityAnimation] forKey:@"kOpacityAnimation"];
    [self setBezierPath];
}

- (void)buildUI {
    
    _titleLabel = [UILabel masLabelWithFont:[UIFont systemFontOfSize:14] lines:1 text:@"" textColor:[UIColor baseTextColor] superView:self constraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(80);
    }];
    
    _textView = [UITextView masTextViewWithFontSize:14 textColor:[UIColor clearColor] placeColor:[UIColor clearColor] placeText:@"" superView:self constraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.left.mas_equalTo(105);
        make.right.mas_equalTo(-40);
    }];
    _textView.tintColor = [UIColor clearColor];
    _textView.backgroundColor = [UIColor clearColor];
    _textView.textColor = [UIColor clearColor];
    _textView.delegate = self;
    
    _textLabel = [UILabel masLabelWithFont:[UIFont systemFontOfSize:14] lines:1 text:@"" textColor:self.placeholderColor superView:self constraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.left.mas_equalTo(105);
        make.right.mas_equalTo(-40);
    }];
    _textFont = [UIFont systemFontOfSize:14];
    
    @weakify(self);
    _lookButton = [UIButton masButtonWithNorImage:[UIImage loadToolImageNamed:@"ic_closeeye"] cornerRadius:0 supView:self constraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.equalTo(self);
        make.width.mas_equalTo(40);
    } touchUp:^(id sender) {
        @strongify(self);
        
        UIButton * button = (UIButton *)sender;
        button.selected = !button.selected;
        if(button.selected) {
            self.textLabel.text = self.textView.text;
        }else {
            self.textLabel.text = self.cipherTextStr;
        }
    }];
    [_lookButton setImage:[UIImage loadToolImageNamed:@"ic_openeye"] forState:UIControlStateSelected];
    _lookButton.hidden = YES;
    
    _lineLayer = [CAShapeLayer layer];
    _lineLayer.fillColor =  [UIColor darkGrayColor].CGColor;
    [_textLabel.layer addSublayer:_lineLayer];
    [_lineLayer addAnimation:[self opacityAnimation] forKey:@"kOpacityAnimation"];
    _lineLayer.hidden = YES;
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = [UIColor baseLineColor];
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(LineHeight);
        make.bottom.equalTo(self.mas_bottom);
    }];
}


- (CABasicAnimation *)opacityAnimation {
    CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnimation.fromValue = @(1.0);
    opacityAnimation.toValue = @(0.0);
    opacityAnimation.duration = 1;
    opacityAnimation.repeatCount = HUGE_VALF;
    opacityAnimation.removedOnCompletion = YES;
    opacityAnimation.fillMode = kCAFillModeForwards;
    opacityAnimation.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    return opacityAnimation;
}

#pragma mark - setter方法
- (void)setTitleStr:(NSString *)titleStr{
    _titleStr = titleStr;
    _titleLabel.text = _titleStr;
}
// 设置键盘类型
- (void)setKeyboardType:(UIKeyboardType)keyboardType {
    _keyboardType = keyboardType;
    _textView.keyboardType = keyboardType;
}

// 设置文字字体
- (void)setTextFont:(UIFont *)textFont {
    _textFont = textFont;
    _textView.font = textFont;
    _textLabel.font = textFont;
}

// 设置文字颜色
- (void)setTextColor:(UIColor *)textColor {
    _textColor = textColor;
    _textLabel.textColor = textColor;
}

// 设置文字，有文字之后，文字颜色改变
- (void)setText:(NSString *)text {
    _text = text;
    
    _textView.text = text;
    _textView.editable = YES;
    if(text && text.length > 0) {
        _lookButton.hidden = NO;
    }else {
        _lookButton.hidden = YES;
    }
    // 脱敏操作
    NSMutableString * textStr = [NSMutableString stringWithString:text];
    if(_fieldType == IKMTextFieldTypeName) {
        for(int i = 0; i < text.length; i++) {
            if(i == 0) {
                continue;
            }
            [textStr replaceCharactersInRange:NSMakeRange(i, 1) withString:@"*"];
        }
    }else if(_fieldType == IKMTextFieldTypePhone) {
        for(int i = 0; i < text.length; i++) {
            if(i < 3) {
                continue;
            }else if(i > 8) {
                break;
            }
            
            [textStr replaceCharactersInRange:NSMakeRange(i, 1) withString:@"*"];
        }
    }else if(_fieldType == IKMTextFieldTypeCertificate) {
        for(int i = 0; i < text.length; i++) {
            [textStr replaceCharactersInRange:NSMakeRange(i, 1) withString:@"*"];
        }
    }
    else if(_fieldType == IKMTextFieldTypenNormal) {
        _textView.editable = NO;
        _lookButton.hidden = YES;
    }
    _cipherTextStr = [NSString stringWithString:textStr];
    _textLabel.text = _cipherTextStr;
    
    if(text && text.length > 0) {
        _textLabel.textColor = _textColor;
    }else {
        _textLabel.textColor = _placeholderColor;
        _textLabel.text = _placeholder;
    }
}

// 设置默认文字
- (void)setPlaceholder:(NSString *)placeholder {
    _placeholder = placeholder;
    
    if(!(_text && _text.length > 0)) {
        _textLabel.text = placeholder;
    }
}

// 设置默认文字颜色
- (void)setPlaceholderColor:(UIColor *)placeholderColor {
    _placeholderColor = placeholderColor;
}

- (void)setTextFieldEnabled:(BOOL)textFieldEnabled{
    _textFieldEnabled = textFieldEnabled;
    _textView.userInteractionEnabled = _textFieldEnabled;
}

- (void)setIsHiddenEye:(BOOL)isHiddenEye {
    _isHiddenEye = isHiddenEye;
    if(isHiddenEye) {
        _lookButton.hidden = YES;
    }else {
        _lookButton.hidden = NO;
    }
}

#pragma mark - UIBezierPath 光标
- (void)setBezierPath {
    CGSize textSize = [@"" getSizeWithFont:_textFont];
    if(self.textView.text && self.textView.text.length > 0) {
       textSize = [_textLabel.text getSizeWithFont:_textFont];
    }
    
    UIBezierPath * path = [UIBezierPath bezierPathWithRect:CGRectMake(textSize.width + 1, (self.frame.size.height - textSize.height) / 2, 1, textSize.height)];
    _lineLayer.path = path.CGPath;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    _lookButton.hidden = YES;
    
    // 获取焦点，直接明文显示, 隐藏小眼睛
    if(textView.text && textView.text.length > 0) {
        _textLabel.text = textView.text;
    }else {
        self.textLabel.textColor = self.placeholderColor;
        self.textLabel.text = self.placeholder;
    }
    
    [self setBezierPath];
    _lineLayer.hidden = NO;
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    if(textView.text && textView.text.length > 0) {
        _textLabel.text = textView.text;
        _textLabel.textColor = self.textColor;
    }else {
        self.textLabel.textColor = self.placeholderColor;
        self.textLabel.text = self.placeholder;
    }
    
    _text = textView.text;
    
    [self setBezierPath];
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    // 隐藏光标
    _lineLayer.hidden = YES;
    
    if(textView.text && textView.text.length > 0) {
        _textLabel.text = textView.text;
    }else {
        self.textLabel.textColor = self.placeholderColor;
        self.textLabel.text = self.placeholder;
    }
    
    return YES;
}

@end
