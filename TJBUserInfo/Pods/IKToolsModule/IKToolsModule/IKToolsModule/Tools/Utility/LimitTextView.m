//
//  LimitTextView
//  IKToolsModule
//
//  Created by 狄烨 . on 2018/12/20.
//  Copyright © 2018 HU. All rights reserved.
//

#import "LimitTextView.h"
#import <Masonry/Masonry.h>
@interface LimitTextView()<UITextViewDelegate>

@end

@implementation LimitTextView

-(instancetype)init{
    self = [super init];
    if (self) {

        [self addSubview:self.textView];
        self.textView.delegate = self;
        [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(self);
        }];
        
        [self addSubview:self.textNumLabel];
        [self.textNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).offset(-3);
            make.right.equalTo(self).offset(-4);
            make.top.equalTo(self.textView.mas_bottom);
        }];
        
        [self addSubview:self.placeholderLabel];
        [self.placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(5);
            make.top.equalTo(self).offset(8);
        }];
    }
    return self;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    if (self.textViewDidChangeBlock) {
        self.textViewDidChangeBlock(textView);
    }
    
    NSString *toBeString = textView.text;
    
    if (toBeString.length > 0) { // 显示隐藏占位符
        self.placeholderLabel.hidden = YES;
    }else {
        self.placeholderLabel.hidden = NO;
    }
    
    if (self.limitLength > 0) { // 限制长度
        //获取高亮部分
        UITextRange *selectedRange = [textView markedTextRange];
        UITextPosition *position = [textView positionFromPosition:selectedRange.start offset:0];
        
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position)
        {
            if (toBeString.length > self.limitLength)
            {
                NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:self.limitLength];
                if (rangeIndex.length == 1)
                {
                    textView.text = [toBeString substringToIndex:self.limitLength];
                }
                else
                {
                    NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, self.limitLength)];
                    textView.text = [toBeString substringWithRange:rangeRange];
                }
            }
        }
    }
    
    /*
     textNumLabel
     */
    if (self.limitLength) {
        self.textNumLabel.text = [NSString stringWithFormat:@"%zd/%zd",textView.text.length,self.limitLength];
        if (textView.text.length > self.limitLength) {
            self.textNumLabel.textColor = [UIColor redColor];
        }else {
            self.textNumLabel.textColor = [UIColor lightGrayColor];
        }
    }else {
        self.textNumLabel.text = [NSString stringWithFormat:@"%zd",textView.text.length];
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if (self.textViewShouldBeginEditingBlock) {
        return self.textViewShouldBeginEditingBlock(textView);
    }
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    if (self.textViewShouldEndEditingBlock) {
        self.textViewShouldEndEditingBlock(textView);
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if (self.textViewDidBeginEditingBlock) {
        self.textViewDidBeginEditingBlock(textView);
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (self.textViewDidEndEditingBlock) {
        self.textViewDidEndEditingBlock(textView);
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if (self.textViewShouldChangeTextInRangeReplacementTextBlock) {
        return self.textViewShouldChangeTextInRangeReplacementTextBlock(textView, range, text);
    }
    return YES;
}

#pragma mark - get

- (UILabel *)placeholderLabel {
    if (_placeholderLabel) return _placeholderLabel;
    _placeholderLabel = [[UILabel alloc] init];
    _placeholderLabel.textColor = [UIColor lightGrayColor];
    _placeholderLabel.font = [UIFont systemFontOfSize:14];
    _placeholderLabel.numberOfLines = 0;
    return _placeholderLabel;
}

- (UILabel *)textNumLabel {
    if (_textNumLabel) return _textNumLabel;
    _textNumLabel = [[UILabel alloc] init];
    _textNumLabel.textColor = [UIColor lightGrayColor];
    _textNumLabel.font = [UIFont systemFontOfSize:14];
    _textNumLabel.textAlignment = NSTextAlignmentRight;
    return _textNumLabel;
}

- (UITextView *)textView {
    if (_textView) return _textView;
    _textView = [[UITextView alloc] init];
    _textView.textColor = [UIColor blackColor];
    _textView.font = [UIFont systemFontOfSize:14];
    return _textView;
}


#pragma markk - set

- (void)setPlaceholder:(NSString *)placeholder {
    _placeholder = placeholder;
    self.placeholderLabel.text = _placeholder;
}

-(void)setLimitLength:(NSInteger)limitLength{
    _limitLength = limitLength;
    self.textNumLabel.text = [NSString stringWithFormat:@"0/%zd",_limitLength];
}
@end
