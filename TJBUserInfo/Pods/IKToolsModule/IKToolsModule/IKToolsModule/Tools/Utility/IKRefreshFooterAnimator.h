//
//  IKRefreshFooterAnimator.h
//  IKToolsModule
//
//  Created by 狄烨 . on 2019/6/4.
//  Copyright © 2019 HU. All rights reserved.
//

#import <MJRefresh/MJRefresh.h>

NS_ASSUME_NONNULL_BEGIN

@interface IKRefreshFooterAnimator : MJRefreshAutoFooter

@end

NS_ASSUME_NONNULL_END
