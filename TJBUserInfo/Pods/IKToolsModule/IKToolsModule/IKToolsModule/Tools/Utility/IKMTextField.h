//
//  IKMTextField.h
//  IKAppointmentModule
//
//  Created by SJZ on 2019/1/2.
//  Copyright © 2019 iOS123. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, IKMTextFieldType) {
    // 姓名
    IKMTextFieldTypeName,
    // 手机号
    IKMTextFieldTypePhone,
    // 证件号
    IKMTextFieldTypeCertificate,
    // 正常
    IKMTextFieldTypenNormal
};

@interface IKMTextField : UIView
// 标题
@property (nonatomic, strong) NSString * titleStr;

// 输入类型
@property (nonatomic, assign) IKMTextFieldType fieldType;

// 键盘类型
@property (nonatomic, assign) UIKeyboardType keyboardType;

// 字体大小
@property (nonatomic, strong) UIFont * textFont;

// 文字颜色
@property (nonatomic, strong) UIColor * textColor;

// 文字
@property (nonatomic, strong) NSString * text;

// 默认文字
@property (nonatomic, strong) NSString * placeholder;

// 默认文字颜色
@property (nonatomic, strong) UIColor * placeholderColor;

// 可编辑状态
@property (nonatomic,assign) BOOL textFieldEnabled;

@property (nonatomic, assign) BOOL isHiddenEye;

// 初始化方法
- (instancetype)initWithType:(IKMTextFieldType)fieldType;
@end

NS_ASSUME_NONNULL_END
