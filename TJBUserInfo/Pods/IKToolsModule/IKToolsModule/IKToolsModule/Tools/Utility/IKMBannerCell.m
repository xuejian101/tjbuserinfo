//
//  IKMBannerCell.m
//  IKToolsModule
//
//  Created by HU on 2018/7/30.
//  Copyright © 2018年 HU. All rights reserved.
//

#import "IKMBannerCell.h"

@implementation IKMBannerCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self addImageView];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.backgroundColor = [UIColor clearColor];
        [self addImageView];
    }
    return self;
}


- (void)addImageView {
    _imageView = [[SDAnimatedImageView alloc] init];
    _imageView.shouldCustomLoopCount = YES;
    _imageView.animationRepeatCount =1;
    [self addSubview:_imageView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _imageView.frame = self.bounds;
}

@end
