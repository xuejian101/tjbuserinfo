//
//  IKMBaseWebViewController.h
//  IKToolsModule
//
//  Created by HU on 2018/7/31.
//  Copyright © 2018年 HU. All rights reserved.
//

#import "IKMBaseViewController.h"
#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface IKMBaseWebViewController : IKMBaseViewController

@property (nonatomic, strong) WKWebView *webView;     //webview
@property (nonatomic, strong) UIButton *reloadBtn;//重新加载的按钮
@property (nonatomic, strong) NSString *navTitle;     //页面标题 
@property (nonatomic, strong) NSString *url;/////初始化请求，不initurl的情况下需要配合loadRequest方法
@property (nonatomic, assign) BOOL isNoNeedLogin;    ////屏蔽Token写入 默认NO支持登录，YES屏蔽写入
@property (nonatomic, strong) NSString *agent;     //自定义UA，不传为默认

#pragma mark - 初始化方法
- (instancetype)initWithLoadURL:(NSString *)urlString;

- (instancetype)initWithURL:(NSURL *)url;

- (instancetype)initWithURL:(NSURL *)url cookie:(NSDictionary *)cookie;

- (instancetype)initWithURLRequest:(NSURLRequest *)requst;

- (void)loadRequest;

- (void)cleanAllWebsiteDataStore;

- (void)closeVC;

- (void)goBackRoot;
@end
