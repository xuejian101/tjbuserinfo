//
//  NSString+Base.h
//  IKToolsModule
//
//  Created by HU on 2018/6/5.
//  Copyright © 2018年 HU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NSString (Base)
- (NSString *)getMD5;
- (NSString *)getBase64;
- (NSString *)stringByTrim;
- (BOOL)empty;
- (BOOL)isNotNull;
- (BOOL)isTelephone;
- (BOOL)isIdentifyNo;
- (CGSize)getSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size;
- (CGSize)getSizeWithFont:(UIFont *)font;
- (CGSize)getSizeWithAtt:(NSDictionary *)att ConstrainedToSize:(CGSize)size;
- (CGSize)getSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size AndLineHeight:(CGFloat)lineHeight;
@end
